import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of } from 'rxjs';
import { catchError, filter, map, mapTo, switchMap, take, tap } from 'rxjs/operators';
import { FirstStageService, ProductAlreadyPresentError } from 'src/app/service/first-stage.service';
import { ModalService } from 'src/app/service/modal.service';
import { SecondStageService } from 'src/app/service/second-stage.service';
import { ThirdStageService } from 'src/app/service/third-stage.service';
import { prodConflictMsg } from 'src/assets/msgs/confirm-msgs';
import {
  submitStageFail, editStageSuccess, resetEditState, editProduct, newStageSuccess,
  submitEditFirstStage, submitEditSecondStage, submitEditThirdStage, submitNewFirstStage, submitNewSecondStage, submitNewThirdStage
} from './product-edit.actions';

@Injectable()
export class ProductEditEffect {

  // SUBMISSIONS //

  readonly submitEditFirstStage$ = createEffect(() => this.actions$.pipe(
    ofType(submitEditFirstStage),
    switchMap(({ id, formValue }) => this.fsService.update(id, formValue).pipe(
      mapTo(editStageSuccess({ id, stage: 1 })),
      catchError((error) => {
        if (error instanceof ProductAlreadyPresentError) {
          return of(submitStageFail({ modalType: 'confirmation', msg: prodConflictMsg, id: error.id }));
        }
        else {
          return of(submitStageFail({ modalType: 'info', msg: { header: error.status, body: error.message } }));
        }
      })
    ))
  ));

  readonly submitEditSecondStage$ = createEffect(() => this.actions$.pipe(
    ofType(submitEditSecondStage),
    switchMap(({ id, formValue }) => this.ssService.update(id, formValue).pipe(
      mapTo(editStageSuccess({ id, stage: 2 })),
      catchError(error => of(submitStageFail({ modalType: 'info', msg: { header: error.status, body: error.message } })))
    ))
  ));

  readonly submitEditThirdStage$ = createEffect(() => this.actions$.pipe(
    ofType(submitEditThirdStage),
    switchMap(({ id, formValue }) => this.tsService.update(id, formValue).pipe(
      mapTo(editStageSuccess({ id, stage: 3 })),
      catchError(error => of(submitStageFail({ modalType: 'info', msg: { header: error.status, body: error.message } })))
    ))
  ));

  readonly submitNewFirstStage$ = createEffect(() => this.actions$.pipe(
    ofType(submitNewFirstStage),
    switchMap(({ formValue }) => this.fsService.save(formValue).pipe(
      map(({ id }) => newStageSuccess({ id, stage: 1 })),
      catchError((error) => {
        if (error instanceof ProductAlreadyPresentError) {
          return of(submitStageFail({ modalType: 'confirmation', msg: prodConflictMsg, id: error.id }));
        }
        else {
          return of(submitStageFail({ modalType: 'info', msg: { header: error.status, body: error.message } }));
        }
      })
    ))
  ));

  readonly submitNewSecondStage$ = createEffect(() => this.actions$.pipe(
    ofType(submitNewSecondStage),
    switchMap(({ id, formValue }) => this.ssService.save(id, formValue).pipe(
      mapTo(newStageSuccess({ id, stage: 2 })),
      catchError((error) => of(submitStageFail({ modalType: 'info', msg: { header: error.status, body: error.message } })))
    )
    )
  ));

  readonly submitNewThirdStage$ = createEffect(() => this.actions$.pipe(
    ofType(submitNewThirdStage),
    switchMap(({ id, formValue }) => this.tsService.save(id, formValue).pipe(
      mapTo(newStageSuccess({ id, stage: 3 })),
      catchError((error) => of(submitStageFail({ modalType: 'info', msg: { header: error.status, body: error.message } })))
    )
    )
  ));

  // SUCCESSES //

  readonly editStageSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(editStageSuccess),
    tap(({ id, stage }) => stage === 2
      ? this.router.navigate(['/product', 'view'], { queryParams: { id, updateImgs: true } })
      : this.router.navigate(['/product', 'view'], { queryParams: { id } })),
    mapTo(resetEditState())
  ));

  readonly newFirstStageSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(newStageSuccess),
    filter(({ stage }) => stage === 1),
    tap(({ id }) => this.router.navigate(['/product', 'edit', id, 2, 'NEW'])),
  ), { dispatch: false });

  readonly newSecondStageSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(newStageSuccess),
    filter(({ stage }) => stage === 2),
    map(({ id }) => editProduct({ prodEditId: id, prodEditType: 'NEW', prodEditStage: 3 }))
  ));

  readonly newThirdStageSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(newStageSuccess),
    filter(({ stage }) => stage === 3),
    tap(({ id }) => this.router.navigate(['/product', 'view'], { queryParams: { id } })),
    mapTo(resetEditState())
  ));

  // FAILINGS //

  readonly submitStageFail$ = createEffect(() => this.actions$.pipe(
    ofType(submitStageFail),
    switchMap(({ modalType, msg, id }) => {
      if (modalType === 'confirmation') {
        return this.modalService.openConfirm({ msg }).events$.pipe(
          take(1),
          switchMap(reply => reply === 'confirm' ? of(null) : EMPTY),
          tap(() => this.router.navigate(['/product', 'view'], { queryParams: { id } })),
          mapTo(resetEditState())
        );
      }
      else {
        return this.modalService.openInfo({ msg }).events$.pipe(
          take(1),
          switchMap(() => EMPTY)
        );
      }
    })
  ));


  constructor(private readonly actions$: Actions,
    private fsService: FirstStageService,
    private ssService: SecondStageService,
    private tsService: ThirdStageService,
    private router: Router,
    private modalService: ModalService) { }
}
