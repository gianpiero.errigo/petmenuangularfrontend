import { createAction, props } from '@ngrx/store';
import { FSDto } from 'src/app/model/fsdto';
import { ModalMsg } from 'src/app/model/modal-params';
import { TSDto } from 'src/app/model/tsdto';

export const editProduct = createAction(
  '[Product Edit] Edit Product',
  props<{ prodEditId: string; prodEditType: 'NEW' | 'MODIFY'; prodEditStage: number }>()
);

export const resetEditState = createAction(
  '[Product Edit] Reset Edit State',
);

export const submitNewFirstStage = createAction(
  '[Product Edit] Submit New Product First Stage',
  props<{ formValue: FSDto }>()
);

export const submitNewSecondStage = createAction(
  '[Product Edit] Submit New Product Second Stage',
  props<{ id: string; formValue: FormData }>()
);

export const submitNewThirdStage = createAction(
  '[Product Edit] Submit New Product Third Stage',
  props<{ id: string; formValue: TSDto }>()
);

export const submitEditFirstStage = createAction(
  '[Product Edit] Submit Edited Product First Stage',
  props<{ id: string; formValue: FSDto }>()
);

export const submitEditSecondStage = createAction(
  '[Product Edit] Submit Edited Product Second Stage',
  props<{ id: string; formValue: FormData }>()
);

export const submitEditThirdStage = createAction(
  '[Product Edit] Submit Edited Product Third Stage',
  props<{ id: string; formValue: TSDto }>()
);

export const newStageSuccess = createAction(
  '[Product Edit] Success Submit New Product Stage',
  props<{ id: string; stage: number }>()
);

export const editStageSuccess = createAction(
  '[Product Edit] Success Submit Edited Product Stage',
  props<{ id: string; stage: number }>()
);

export const submitStageFail = createAction(
  '[Product Edit] Failing Submit Edited Product Stage',
  props<{ modalType: 'info' | 'confirmation'; msg: ModalMsg; id?: string }>()
);


