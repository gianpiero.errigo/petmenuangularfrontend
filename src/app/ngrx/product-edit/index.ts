import { createFeatureSelector } from '@ngrx/store';

export interface ProductEditState {
	readonly prodEditId: string;
	readonly prodEditType: '' | 'NEW' | 'MODIFY';
	readonly prodEditStage: number;
}

export const getProductEditState = createFeatureSelector<ProductEditState>('productEdit');
