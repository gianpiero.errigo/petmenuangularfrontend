import { createSelector } from '@ngrx/store';
import { getProductEditState } from '.';

export const getProductEditId = createSelector(getProductEditState, state => state.prodEditId);

export const getProductEditStage = createSelector(getProductEditState, state => state.prodEditStage);

export const getProductEditType = createSelector(getProductEditState, state => state.prodEditType);
