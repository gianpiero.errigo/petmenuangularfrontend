import { createReducer, on } from '@ngrx/store';
import { ProductEditState } from '.';
import { editProduct, resetEditState } from './product-edit.actions';

export const initialState: ProductEditState = {prodEditId: '', prodEditType: '', prodEditStage: 0};

export const productEditReducer = createReducer(
  initialState,
  on(editProduct, (state, { prodEditId, prodEditType, prodEditStage }) => ({...state, prodEditId, prodEditType, prodEditStage})),
  on(resetEditState,  () => ({...initialState})),
);

