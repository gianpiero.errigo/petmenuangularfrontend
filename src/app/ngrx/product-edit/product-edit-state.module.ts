import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ProductEditEffect } from './product-edit.effects';
import { productEditReducer } from './product-edit.reducers';

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature('productEdit', productEditReducer),
    EffectsModule.forFeature([ProductEditEffect]),
  ],
  exports: []
})
export class ProductEditStateModule { }
