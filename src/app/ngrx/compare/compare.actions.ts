import { createAction, props } from '@ngrx/store';
import { Product } from 'src/app/model/product';

export const toggleComparePanel = createAction(
  '[Compare] Toggle Panel'
);

export const hideComparePanel = createAction(
  '[Compare] Hide Panel'
);

export const showComparePanel = createAction(
  '[Compare] Show Panel'
);

export const toggleCheckedForCompare = createAction(
  '[Compare] Toggle Product Selection',
  props<{ product: Product; posX: number; posY: number }>()
);

export const addToCompare = createAction(
  '[Compare] Add Product To List',
  props<{ product: Product }>()
);

export const rmFromCompare = createAction(
  '[Compare] Remove Product From List',
  props<{ product: Product }>()
);

export const clearCompare = createAction(
  '[Compare] Clear List',
);

