import { Product } from 'src/app/model/product';

export interface CompareState {
	readonly products: Array<Product>;
	compareShown: boolean;
}
