import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CompareEffect } from './compare-effects';
import { compareReducer } from './compare.reducers';


@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature('compare', compareReducer),
    EffectsModule.forFeature([CompareEffect]),
  ],
  exports: []
})
export class CompareStateModule { }
