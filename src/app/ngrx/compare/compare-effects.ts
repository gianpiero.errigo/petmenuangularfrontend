import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { CompareProductsService } from 'src/app/service/compare-products.service';
import { addToCompare, rmFromCompare, toggleCheckedForCompare } from './compare.actions';
import { getComparedProducts, isCompareFull } from './compare.selectors';

@Injectable()
export class CompareEffect {

  readonly toggleCompare$ = createEffect(() => this.actions$.pipe(
    ofType(toggleCheckedForCompare),
    concatLatestFrom(({product}) =>	[
      this.appState.select(getComparedProducts).pipe(
        map(comparedProducts =>comparedProducts.some(compared => compared.id ===  product.id))
      ),
      this.appState.select(isCompareFull)
    ]),
    map(([{product, posX, posY}, checked, listFull]) => {
      if(checked) {
        this.appState.dispatch(rmFromCompare({product}));
      }
      else {
        if(!listFull) {
          this.appState.dispatch(addToCompare({product}));
        }
        else {
          this.compareService.notifyNoAddToFullList(posX, posY);
        }
      }
    })
  ), { dispatch: false });
  //      // dispatch searchResultsFailure if there's an error
  // catchError(error => of(searchResultsFailure({ query, error })))


  constructor(private actions$: Actions,
							private appState: Store,
							private compareService: CompareProductsService) {}
}
