import { createReducer, on } from '@ngrx/store';
import { CompareState } from '.';
import { addToCompare, clearCompare, hideComparePanel,
  rmFromCompare, showComparePanel, toggleComparePanel } from './compare.actions';

export const initialState: CompareState = { products: [], compareShown: false, };

export const compareReducer = createReducer(
  initialState,
  on(addToCompare, (state, { product }) => ({...state, products: [...state.products, product]})),
  on(rmFromCompare, (state, { product }) => ({
    ...state,
    products: state.products.filter(compared => compared.id !== product.id),
    compareShown: (state.products.length === 1) ? false : state.compareShown
  })),
  on(clearCompare, state => ({...state, products: [], compareShown: false})),
  on(toggleComparePanel, state => ( state.products.length ? {...state, compareShown: !state.compareShown} : {...state})),
  on(hideComparePanel, state => ( {...state, compareShown: false})),
  on(showComparePanel, state => ( state.products.length ? {...state, compareShown: true} : {...state})),
);

