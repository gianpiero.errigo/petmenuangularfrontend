import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CompareState } from '.';

const MAX_PRODUCTS = 4;
const compareFeatureSelector = createFeatureSelector<CompareState>('compare');

export const isCompareShown = createSelector(
  compareFeatureSelector,
  state => state.compareShown
);

export const isCompareEmpty = createSelector(
  compareFeatureSelector,
  state => state.products.length === 0
);

export const isCompareFull = createSelector(
  compareFeatureSelector,
  state => state.products.length === MAX_PRODUCTS
);

export const getCompareLength = createSelector(
  compareFeatureSelector,
  state => state.products.length
);

export const getComparedProducts = createSelector(
  compareFeatureSelector,
  state => state.products
);
