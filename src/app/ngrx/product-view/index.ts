import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import { SearchParams } from 'src/app/model/app-state';
import { Product } from 'src/app/model/product';
import { listProductsReducer, showProductReducer } from './product-view.reducers';

export interface ProductViewState {
	readonly showProduct: ShowProductState;
	readonly listProducts: ListProductsState;
}

export interface ListProductsState {
	readonly searchParams: SearchParams;
	readonly foundProducts: Array<Product>;
}

export interface ShowProductState {
	readonly shownProdId: string;
	readonly shownProduct: Product | undefined;
}

export const getProductViewState = createFeatureSelector<ProductViewState>('productView');

export const productViewReducers: ActionReducerMap<ProductViewState> = {
  showProduct: showProductReducer,
  listProducts: listProductsReducer,
};
