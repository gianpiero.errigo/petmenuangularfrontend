import { createSelector } from '@ngrx/store';
import { getProductViewState } from '.';

export const getListProducts = createSelector(getProductViewState, state => state.listProducts);

export const getSearchParams = createSelector(getListProducts, state => state.searchParams);

export const getSearchResults = createSelector(getListProducts, state => state.foundProducts);

export const getSearch = createSelector(
  getSearchParams,
  getSearchResults,
  (params, products) => (products.length !== 0) ? {params, products} : null
);

export const getShowProduct = createSelector(getProductViewState, state => state.showProduct);

export const getShownProdId = createSelector(getShowProduct, state => state.shownProdId);

export const getShownProduct = createSelector(getShowProduct, state => state.shownProduct);
