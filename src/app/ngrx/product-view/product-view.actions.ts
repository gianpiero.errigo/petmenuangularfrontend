import { createAction, props } from '@ngrx/store';
import { SearchParams } from 'src/app/model/app-state';
import { Product } from 'src/app/model/product';

export const searchProducts = createAction(
  '[List Products] Search Products',
  props<{ searchParams: SearchParams }>()
);

export const searchProductsSuccess = createAction(
  '[List Products] Search Products Success',
  props<{ readonly foundProducts: Array<Product> }>()
);

export const showProduct = createAction(
  '[Show Product] Show Product',
  props<{ shownProdId: string }>()
);

export const showProductSuccess = createAction(
  '[Show Product] Show Product Success',
  props<{ shownProdId: string; shownProduct: Product }>()
);
