import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { ProdService } from 'src/app/service/prod.service';
import { searchProductsSuccess, searchProducts, showProduct, showProductSuccess } from './product-view.actions';

@Injectable()
export class ProductViewEffect {

  readonly searchProducts$ = createEffect(() => this.actions$.pipe(
    ofType(searchProducts),
    switchMap(({ searchParams }) => this.prodService.getProductsByMainTraits(searchParams).pipe(
      map(foundProducts => searchProductsSuccess({ foundProducts }))
      // dispatch searchResultsFailure if there's an error
      // catchError(error => of(searchResultsFailure({ query, error })))
    ))
  ));

  readonly showProduct$ = createEffect(() => this.actions$.pipe(
    ofType(showProduct),
    switchMap(({ shownProdId }) => this.prodService.getProductById(shownProdId).pipe(
      map(shownProduct => showProductSuccess({ shownProdId, shownProduct }))
      // dispatch searchResultsFailure if there's an error
      // catchError(error => of(searchResultsFailure({ query, error })))
    ))
  ));

  constructor(private actions$: Actions,
		private prodService: ProdService) {}
}
