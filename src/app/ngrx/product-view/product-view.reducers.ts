import { createReducer, on } from '@ngrx/store';
import { initSearchParams } from 'src/app/model/app-state';
import { ListProductsState, ShowProductState } from '.';
import { showProduct, showProductSuccess, searchProductsSuccess, searchProducts } from './product-view.actions';

export const initialListState: ListProductsState = {searchParams: initSearchParams(), foundProducts: []};
export const listProductsReducer = createReducer(
  initialListState,
  on(searchProducts, (state, { searchParams }) => ({...state, searchParams, foundProducts: []})),
  on(searchProductsSuccess, (state, { foundProducts }) => ({...state, foundProducts}))
);

export const initialShowState: ShowProductState = {shownProdId: '', shownProduct: undefined};
export const showProductReducer = createReducer(
  initialShowState,
  on(showProduct, (state, { shownProdId }) => ({...state, shownProdId})),
  on(showProductSuccess, (state, { shownProdId, shownProduct }) => ({...state, shownProdId, shownProduct})),
);
