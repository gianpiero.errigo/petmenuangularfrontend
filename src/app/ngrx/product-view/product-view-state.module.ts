import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { productViewReducers } from '.';
import { ProductViewEffect } from './product-view.effects';

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature('productView', productViewReducers),
    EffectsModule.forFeature([ProductViewEffect]),
  ],
  exports: []
})
export class ProductViewStateModule { }
