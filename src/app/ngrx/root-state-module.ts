import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { localStorageSync } from 'ngrx-store-localstorage';
import { environment } from 'src/environments/environment';
import { compareReducer } from './compare/compare.reducers';
import { historyReducer } from './history/history.reducers';
import { MainSpinnerEffect } from './main-spinner/main-spinner.effects';
import { mainSpinnerReducer } from './main-spinner/main-spinner.reducers';

export const localStorageSyncReducer =
  (reducer: ActionReducer<any>): ActionReducer<any> =>
    localStorageSync({ keys: ['compare', 'history'], rehydrate: true })(reducer);

const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forRoot({ compare: compareReducer, history: historyReducer, mainSpinner: mainSpinnerReducer }, { metaReducers }),
    EffectsModule.forRoot([MainSpinnerEffect]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  exports: []
})
export class RootStateModule { }
