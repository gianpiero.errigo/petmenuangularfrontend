import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { mapTo } from 'rxjs/operators';
import {
  submitEditFirstStage, submitEditSecondStage, submitEditThirdStage,
  submitStageFail, editStageSuccess, newStageSuccess,
  submitNewFirstStage, submitNewSecondStage, submitNewThirdStage
} from '../product-edit/product-edit.actions';
import { searchProducts, searchProductsSuccess, showProduct, showProductSuccess } from '../product-view/product-view.actions';
import { addLoading, rmLoading } from './main-spinner.actions';

@Injectable()
export class MainSpinnerEffect {

  readonly mainSpinnerOn = createEffect(() => this.actions$.pipe(
    ofType(showProduct, searchProducts,
      submitEditFirstStage, submitEditSecondStage, submitEditThirdStage,
      submitNewFirstStage, submitNewSecondStage, submitNewThirdStage),
    mapTo(addLoading())
  ));

  readonly mainSpinnerOff = createEffect(() => this.actions$.pipe(
    ofType(showProductSuccess, searchProductsSuccess, editStageSuccess, newStageSuccess, submitStageFail),
    mapTo(rmLoading())
  ));

  constructor(private readonly actions$: Actions) { }
}
