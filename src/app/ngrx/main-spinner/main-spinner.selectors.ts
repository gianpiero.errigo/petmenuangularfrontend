import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MainSpinnerState } from '.';

const mainSpinnerFeatureSelector = createFeatureSelector<MainSpinnerState>('mainSpinner');

export const getLoadingQueue = createSelector(mainSpinnerFeatureSelector, state => state.loadingQueue);
