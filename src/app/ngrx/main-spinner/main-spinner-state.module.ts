import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { mainSpinnerReducer } from './main-spinner.reducers';


@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature('mainSpinner', mainSpinnerReducer),
  ],
  exports: []
})
export class MainSpinnerStateModule { }
