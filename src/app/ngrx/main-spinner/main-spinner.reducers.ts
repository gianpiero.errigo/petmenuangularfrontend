import { createReducer, on } from '@ngrx/store';
import { MainSpinnerState } from '.';
import { addLoading, rmLoading } from './main-spinner.actions';

export const initialState: MainSpinnerState = {loadingQueue: 0};
export const mainSpinnerReducer = createReducer(
  initialState,
  on(addLoading, (state) => ({...state, loadingQueue: state.loadingQueue+1})),
  on(rmLoading, (state) => ({...state, loadingQueue: state.loadingQueue-1}))
);
