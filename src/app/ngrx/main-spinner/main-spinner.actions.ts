import { createAction } from '@ngrx/store';

export const addLoading = createAction(
  '[Main Spinner] Increment Queue'
);

export const rmLoading = createAction(
  '[Main Spinner] Decrement Queue'
);
