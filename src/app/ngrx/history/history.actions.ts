import { createAction, props } from '@ngrx/store';

export const setInitial = createAction(
  '[Navigation Service] Set Navigation as Initial or Not',
  props<{ initial: boolean }>()
);

