import { createReducer, on } from '@ngrx/store';
import { HistoryState } from '.';
import { setInitial } from './history.actions';

export const initialState: HistoryState = { initialNavigation: true };

export const historyReducer = createReducer(
  initialState,
  on(setInitial, (state, { initial }) => ({...state, initialNavigation: initial})),
);
