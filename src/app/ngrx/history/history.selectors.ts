import { createFeatureSelector, createSelector } from '@ngrx/store';
import { HistoryState } from '.';

const historyFeatureSelector = createFeatureSelector<HistoryState>('history');

export const isInitialNavigation = createSelector(
  historyFeatureSelector,
  state => state.initialNavigation
);
