import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { historyReducer } from './history.reducers';

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature('history', historyReducer),
  ],
  exports: []
})
export class HistoryStateModule { }
