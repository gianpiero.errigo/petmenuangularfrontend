import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ReplaySubject, EMPTY } from 'rxjs';
import { filter, take, switchMap, catchError } from 'rxjs/operators';
import { Product } from '../model/product';
import { environment } from '../../environments/environment';
import { ModalService } from './modal.service';
import { prodDeleteConfirmMsg } from 'src/assets/msgs/confirm-msgs';
import { prodDeleteFailInfoMsg, prodDeleteSuccessInfoMsg } from 'src/assets/msgs/info-msgs';
import { SearchParams } from '../model/app-state';


@Injectable({
  providedIn: 'root'
})
export class ProdService {

  product$: ReplaySubject<Product> = new ReplaySubject();

  private productUrl: string;
  private headers: HttpHeaders;

  constructor(private http: HttpClient,
    private modalService: ModalService) {

    this.productUrl = `${environment.pmBackUrl}/product`;
    this.headers = new HttpHeaders({ 'content-type': 'application/json' });
  }

  public getProductById(id: string) {
    return this.http.get<Product>(this.productUrl + '/' + id, { headers: this.headers });
  }

  public getProductsByMainTraits(mainTraits: SearchParams) {
    return this.http.get<Array<Product>>(
      this.productUrl + '/search/', { headers: this.headers, params: new HttpParams({ fromObject: { ...mainTraits } }) }
    );
  }

  public getRecipesByLine(brand: string, line: string) {
    return this.http.get<Array<string>>(this.productUrl + '/search/firstStageByLine', { headers: this.headers, params: { brand, line } });
  }

  public deleteProduct(id: string) {
    return this.modalService.openConfirm({ msg: prodDeleteConfirmMsg }).events$.pipe(
      filter(reply => !!reply),
      take(1),
      switchMap(reply => reply === 'confirm' ? this.http.delete(this.productUrl + '/' + id, { headers: this.headers }) : EMPTY),
      catchError(() => this.modalService.openInfo({ msg: prodDeleteFailInfoMsg }).events$.pipe(
        take(1),
        switchMap(() => EMPTY)
      )),
      switchMap(() => this.modalService.openInfo({ msg: prodDeleteSuccessInfoMsg }).events$)
    );
  }

}

