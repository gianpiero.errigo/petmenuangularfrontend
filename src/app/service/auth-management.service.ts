import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthManagementService {

  private authManagementUrl: string;

  constructor(private http: HttpClient) {
    this.authManagementUrl = `${environment.pmBackUrl}/auth`;
  }

  public changeName(newName: string): Observable<void> {
    return this.http.patch<void>(this.authManagementUrl + '/change/name', newName);
  }
}
