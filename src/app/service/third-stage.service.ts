import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TSDto } from '../model/tsdto';

@Injectable({
  providedIn: 'root'
})
export class ThirdStageService {

  private productUrl: string;

  constructor(private http: HttpClient) {
    this.productUrl = `${environment.pmBackUrl}/product`;
  }

  public save(id: string, tsDto: TSDto) {
    return this.http.post(this.productUrl + '/' + id + '/thirdstage', tsDto);
  }

  public get(id: string) {
    return this.http.get<TSDto>(this.productUrl + '/' + id + '/thirdstage');
  }

  public update(id: string, tsDto: TSDto) {
    return this.http.put(this.productUrl + '/' + id + '/thirdstage', tsDto);
  }


}
