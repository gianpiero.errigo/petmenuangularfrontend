import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserCommit, UserCommitDto } from '../model/user-commit';
import { concatAll, map, toArray } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ListUserCommitsService {

  private userUrl: string;

  constructor(private http: HttpClient) {
    this.userUrl = `${environment.pmBackUrl}/user`;
  }

  public get(userName: string) {
    return this.http.get<Array<UserCommitDto>>(`${this.userUrl}/${userName}/commitlist`).pipe(
      concatAll(),
      map(({ time, ...userCommit }) => ({ ...userCommit, time: new Date(time) } as UserCommit)),
      toArray()
    );
  }
}
