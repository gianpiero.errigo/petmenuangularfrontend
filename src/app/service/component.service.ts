import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComponentService {

  private componentUrl: string;

  constructor(private http: HttpClient) {
    this.componentUrl = `${environment.pmBackUrl}/component`;
  }

  public get(): Observable<string[]> {
    return this.http.get<string[]>(this.componentUrl + '/search/');
  }
}

