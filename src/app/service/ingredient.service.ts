import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  private ingredientUrl: string;

  constructor(private http: HttpClient) {
    this.ingredientUrl = `${environment.pmBackUrl}/ingredient`;
  }

  public get(): Observable<string[]> {
    return this.http.get<string[]>(this.ingredientUrl + '/search/');
  }
}

