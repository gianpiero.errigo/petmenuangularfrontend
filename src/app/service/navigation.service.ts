import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationStart } from '@angular/router';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { setInitial } from '../ngrx/history/history.actions';
import { isInitialNavigation } from '../ngrx/history/history.selectors';

@Injectable({ providedIn: 'root' })
export class NavigationService {

  constructor(private router: Router,
    private appState: Store,
    private location: Location) {


    this.router.events.pipe(
      filter((event): event is NavigationStart => event instanceof NavigationStart))
      .subscribe((nav: NavigationStart) => (nav.id === 1 || nav.restoredState?.navigationId === 1)
        ? this.appState.dispatch(setInitial({ initial: true }))
        : this.appState.dispatch(setInitial({ initial: false }))
      );
  }

  inAppBack(): void {
    this.appState.select(isInitialNavigation).pipe(
      filter(isInitial => !isInitial))
      .subscribe(() => this.location.back());

  }
}
