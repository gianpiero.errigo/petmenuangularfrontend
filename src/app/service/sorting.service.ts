import { Injectable } from '@angular/core';
import { Analysis } from '../model/analysis';
import { IngredientQuota } from '../model/ingredient-quota';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class SortingService {

  sortByIng(ing: string, productsList: Array<Product>) {

    let matching = productsList.filter(
      prod => prod.tsDto && prod.tsDto.ingQuotasList && prod.tsDto.ingQuotasList.some(
        (iq: IngredientQuota) => iq.ingredient === ing)
    );

    const unmatching = productsList.filter(
      prod => !matching.includes(prod)
    );

    matching = matching.sort((a, b) =>
      (b.tsDto.ingQuotasList.find(iq => iq.ingredient === ing)?.ingPerc ?? 0)
      -
      (a.tsDto.ingQuotasList.find(iq => iq.ingredient === ing)?.ingPerc ?? 0)
    );

    return matching.concat(unmatching);

  }

  sortByAnalysis(element: keyof Analysis, productsList: Array<Product>) {

    let matching = productsList.filter(
      prod => prod.tsDto && prod.tsDto.analysis && prod.tsDto.analysis[element]
    );

    const unmatching = productsList.filter(
      prod => !matching.includes(prod)
    );

    matching = matching.sort((a, b) =>
      (b.tsDto.analysis[element] ?? 0) - (a.tsDto.analysis[element] ?? 0));

    return matching.concat(unmatching);

  }

}
