import { TestBed } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs';

import { ProdService } from './prod.service';
import { ModalService } from './modal.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { prodDeleteFailInfoMsg, prodDeleteSuccessInfoMsg } from 'src/assets/msgs/info-msgs';
import { ModalParams } from '../model/modal-params';


describe('ProdService', () => {

  let service: ProdService;
  let modal: ModalService;
  let httpTestingController: HttpTestingController;
  const modalMock = {
    openConfirm: () => ({ events$: confirmSub$ }),
    openInfo: (_: ModalParams) => ({ events$: new BehaviorSubject<string|null>(null) })
  };

  let confirmSub$: BehaviorSubject<string>;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: ModalService, useValue: modalMock },
      ]
    });

    service = TestBed.inject(ProdService);
    modal = TestBed.inject(ModalService);
    httpTestingController = TestBed.inject(HttpTestingController);

    spyOn(modal, 'openInfo').and.callThrough();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should issue delete request on user confirmation and show successful info dialog', () => {
    confirmSub$ = new BehaviorSubject('confirm');

    service.deleteProduct('1').subscribe();

    const req = httpTestingController.expectOne(`${environment.pmBackUrl}/product/1`);
    req.flush('OK');

    expect(modal.openInfo).toHaveBeenCalledWith({ msg: prodDeleteSuccessInfoMsg });
  });

  it('should cancel delete request on any other situation', () => {
    confirmSub$ = new BehaviorSubject('anythingButConfirm');

    service.deleteProduct('1').subscribe();

    httpTestingController.expectNone(`${environment.pmBackUrl}/product/1`);

    expect(modal.openInfo).not.toHaveBeenCalled();
  });

  it('should issue delete request and show fail info dialog on request error', () => {
    confirmSub$ = new BehaviorSubject('confirm');

    service.deleteProduct('1').subscribe();

    const req = httpTestingController.expectOne(`${environment.pmBackUrl}/product/1`);
    req.flush('Network error', { status: 404, statusText: 'Not Found' });

    expect(modal.openInfo).toHaveBeenCalledWith({ msg: prodDeleteFailInfoMsg });
  });

});
