import { Injectable, Injector, ElementRef } from '@angular/core';
import { Overlay, OverlayConfig, PositionStrategy, ConnectionPositionPair, OverlayRef } from '@angular/cdk/overlay';
import { ModalParams, ModalContent } from '../model/modal-params';
import { ComponentPortal } from '@angular/cdk/portal';
import { ModalPresenterComponent } from '../share/modal/modal-presenter/modal-presenter.component';
import { ModalRef } from 'src/app/share/modal/modal-ref';
import { ConfirmModalComponent } from '../share/modal/confirm-modal/confirm-modal.component';
import { InfoModalComponent } from '../share/modal/info-modal/info-modal.component';
import { Top } from '../model/modal-positions';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  
  overlayRef!: OverlayRef;

  constructor(private overlay: Overlay, private injector: Injector) { }

  openHelpPopup({ origin, positions, content }: ModalParams) {
    const overlayRef = this.overlay.create(new OverlayConfig({
      maxHeight: '60%',
      maxWidth: '90%',
      positionStrategy: this.getPanelPosition(origin, positions),
      scrollStrategy: this.overlay.scrollStrategies.reposition(),
      hasBackdrop: true,
    }));

    return this.attachModal(content, overlayRef, { closingBackdrop: true, animatorTpl: 'help' });
  }

  openDropdown({ origin, positions, content }: ModalParams) {

    const overlayRef = this.overlay.create(new OverlayConfig({
      maxWidth: '90%',
      minWidth: '8rem',
      positionStrategy: this.getPanelPosition(origin, positions),
      scrollStrategy: this.overlay.scrollStrategies.close(),
    }));

    return this.attachModal(content, overlayRef, { closingOutClick: true, animatorTpl: 'dropDown' });
  }

  openTooltip({ origin, positions, content, offsetX, offsetY }: ModalParams) {
    const overlayRef = this.overlay.create(new OverlayConfig({
      positionStrategy: this.getPanelPosition(origin, positions, offsetX, offsetY),
      scrollStrategy: this.overlay.scrollStrategies.close(),
    }));

    return this.attachModal(content, overlayRef, { closingOutClick: true });
  }

  openSpinner({ content }: ModalParams) {

    const overlayRef = this.overlay.create(new OverlayConfig({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
      backdropClass: 'spinner-backdrop',
    }));

    return this.attachModal(content, overlayRef, { closingBackdrop: false });
  }

  openConfirm({ msg }: ModalParams) {

    const overlayRef = this.overlay.create(new OverlayConfig({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      scrollStrategy: this.overlay.scrollStrategies.block(),
      hasBackdrop: true,
      backdropClass: 'spinner-backdrop',
    }));

    return this.attachModal(ConfirmModalComponent, overlayRef, { msg });
  }

  openInfo({ msg }: ModalParams) {

    const overlayRef = this.overlay.create(new OverlayConfig({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      scrollStrategy: this.overlay.scrollStrategies.block(),
      hasBackdrop: true,
      backdropClass: 'spinner-backdrop',
    }));

    return this.attachModal(InfoModalComponent, overlayRef, { msg });
  }

  private attachModal(content: ModalContent = '', newOverlay: OverlayRef, data: ModalParams = {}) {
    
    this.overlayRef?.dispose();
    this.overlayRef = newOverlay;
    const modalRef = new ModalRef(this.overlayRef, content, data);
    const injector = Injector.create({
      parent: this.injector,
      providers: [{ provide: ModalRef, useValue: modalRef }]
    });

    this.overlayRef.attach(new ComponentPortal(ModalPresenterComponent, null, injector));
    return modalRef;
  }

  private getPanelPosition(
    origin: ElementRef | Element | { x: number; y: number } | undefined = { x: 0, y: 0 },
    positions: ConnectionPositionPair[] = [Top],
    offsetX = 0,
    offsetY = 0
  ): PositionStrategy {
    return this.overlay.position()
      .flexibleConnectedTo(origin)
      .withPositions(positions)
      .withViewportMargin(5)
      .withDefaultOffsetX(offsetX)
      .withDefaultOffsetY(offsetY)
      .withFlexibleDimensions(true)
      .withLockedPosition(true)
      .withPush(true);
  }

}
