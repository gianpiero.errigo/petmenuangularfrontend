import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User, UserDto } from '../model/user';
import { concatAll, map, toArray } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ListUsersService {

  private userUrl: string;

  constructor(private http: HttpClient) {
    this.userUrl = `${environment.pmBackUrl}/user`;
  }

  public get() {
    return this.http.get<Array<UserDto>>(this.userUrl + '/list').pipe(
      concatAll(),
      map(({ lastCommit, ...user }) => ({ ...user, lastCommit: new Date(lastCommit) } as User)),
      toArray()
    );
  }
}
