import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  private brandUrl: string;

  constructor(private http: HttpClient) {
    this.brandUrl = `${environment.pmBackUrl}/brand`;
  }

  public get(): Observable<string[]> {
    return this.http.get<string[]>(this.brandUrl + '/search/');
  }
}
