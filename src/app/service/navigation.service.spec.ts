import { TestBed } from '@angular/core/testing';
import { NavigationStart, Router, RouterEvent } from '@angular/router';
import { ReplaySubject } from 'rxjs';

import { NavigationService } from './navigation.service';
import { setInitial } from '../ngrx/history/history.actions';
import { Action, Store } from '@ngrx/store';


describe('NavigationService', () => {
  let service: NavigationService;
  let store: Store;
  const storeMock = {
    dispatch: (_: Action) => ({ })
  };
  const eventsSub$ = new ReplaySubject<RouterEvent>();

  beforeEach(() => {

    const routerSpyObj = jasmine.createSpyObj('Router', {}, { events: eventsSub$ });

    TestBed.configureTestingModule({
      providers: [
        { provide: Store, useValue: storeMock },
        { provide: Router, useValue: routerSpyObj }
      ]
    });

    service = TestBed.inject(NavigationService);
    store = TestBed.inject(Store);

    spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set navigation as initial when nav id is 1', () => {
    eventsSub$.next(new NavigationStart(1, ''));
    expect(store.dispatch).toHaveBeenCalledWith(setInitial({ initial: true }));
  });

  it('should set navigation as initial when nav gets restored to id 1', () => {
    eventsSub$.next(new NavigationStart(99, '', 'popstate', { navigationId: 1 }));
    expect(store.dispatch).toHaveBeenCalledWith(setInitial({ initial: true }));
  });

  it('should NOT set navigation as initial when id or restored id are not 1', () => {
    eventsSub$.next(new NavigationStart(99, '', 'popstate', { navigationId: 88 }));
    expect(store.dispatch).toHaveBeenCalledWith(setInitial({ initial: false }));
  });

});
