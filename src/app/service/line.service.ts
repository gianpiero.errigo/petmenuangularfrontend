import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LineService {

  private lineUrl: string;

  constructor(private http: HttpClient) {
    this.lineUrl = `${environment.pmBackUrl}/line`;
  }

  public getFromBrand(brand: string): Observable<string[]> {
    return this.http.get<string[]>(this.lineUrl + '/search/', { params: { brand } });
  }
}
