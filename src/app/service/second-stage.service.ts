import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { SSDto } from '../model/ssdto';

@Injectable({
  providedIn: 'root'
})
export class SecondStageService {

  private productUrl: string;

  constructor(private http: HttpClient) {
    this.productUrl = `${environment.pmBackUrl}/product`;
  }

  public save(id: string, images: FormData) {
    return this.http.post<{ id: string; _stage: number }>(this.productUrl + '/' + id + '/secondstage', images);
  }

  public get(id: string) {
    return this.http.get<SSDto>(this.productUrl + '/' + id + '/secondstage');
  }

  public update(id: string, images: FormData) {
    return this.http.put<string>(this.productUrl + '/' + id + '/secondstage', images);
  }
}
