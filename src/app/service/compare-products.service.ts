import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { compareListFullMsg } from 'src/assets/msgs/tooltip-msgs';
import { Top } from '../model/modal-positions';
import { ModalService } from './modal.service';

@Injectable({
  providedIn: 'root'
})
export class CompareProductsService {

  public wobbleButton$ = new BehaviorSubject(false);

  constructor(private modalService: ModalService) { }

  public notifyNoAddToFullList(posX: number, posY: number) {
    this.wobbleButton();
    this.showListFullTooltip(posX, posY);
  }

  private wobbleButton() {
    this.wobbleButton$.next(true);
  }

  private showListFullTooltip(posX: number, posY: number) {
    this.modalService.openTooltip({
      origin: { x: posX, y: posY },
      positions: [Top],
      offsetY: -20,
      content: compareListFullMsg.header,
    });
  }

}
