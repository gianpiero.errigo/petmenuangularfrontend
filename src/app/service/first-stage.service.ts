import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { FSDto } from '../model/fsdto';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirstStageService {

  private productUrl: string;

  constructor(private http: HttpClient) {
    this.productUrl = `${environment.pmBackUrl}/product`;
  }

  public save(fsinput: FSDto) {
    return this.http.post<{ id: string; _stage: number }>(this.productUrl, fsinput).pipe(
      catchError((response: HttpErrorResponse) =>
        throwError(response.status === 409 ? new ProductAlreadyPresentError(response.error.detail) : response))
    );
  }

  public get(id: string) {
    return this.http.get<FSDto>(this.productUrl + '/' + id + '/firststage');
  }

  public update(id: string, fsinput: FSDto) {
    return this.http.put<string>(this.productUrl + '/' + id + '/firststage', fsinput).pipe(
      catchError((response: HttpErrorResponse) =>
        throwError(response.status === 409 ? new ProductAlreadyPresentError(response.error.detail) : response))
    );
  }

}

export class ProductAlreadyPresentError extends Error {
  id: string;
  constructor(conflictProdId: string) {
    super();
    this.id = conflictProdId;
  }
}
