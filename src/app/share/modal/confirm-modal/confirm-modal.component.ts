import { Component, OnInit } from '@angular/core';
import { ModalRef } from '../modal-ref';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {

  header: string | undefined;
  body: string | undefined;
  constructor(private modalRef: ModalRef) { }

  ngOnInit(): void {
    this.header = this.modalRef.data.msg?.header;
    this.body = this.modalRef.data.msg?.body;
  }

  onConfirm() {
    this.modalRef.events$.next('confirm');
    this.modalRef.detach();
  }

  onCancel() {
    this.modalRef.events$.next('cancel');
    this.modalRef.detach();
  }

}
