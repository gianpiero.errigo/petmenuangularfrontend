import { OverlayRef } from '@angular/cdk/overlay';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReplaySubject } from 'rxjs';
import { ModalRef } from '../modal-ref';

import { ConfirmModalComponent } from './confirm-modal.component';
import { ConfirmModalModule } from './confirm-modal.module';

describe('InfoModalComponent', () => {
  let fixture: ComponentFixture<ConfirmModalComponent>;
  let modalRefMock: ModalRef;
  let overlayMock: OverlayRef;
  let events$Spy: jasmine.Spy;
  let detachSpy: jasmine.Spy;

  beforeEach(() => {

    modalRefMock = {
      content: '',
      data: { msg: { header: 'header', body: 'body' } },
      events$: new ReplaySubject<string>(),
      overlay: overlayMock, detach: () => null
    };
    events$Spy = spyOn(modalRefMock.events$, 'next');
    detachSpy = spyOn(modalRefMock, 'detach');


    TestBed.configureTestingModule({
      imports: [ConfirmModalModule],
      providers: [{ provide: ModalRef, useValue: modalRefMock }]
    });

    fixture = TestBed.createComponent(ConfirmModalComponent);
    fixture.detectChanges();
  });

  it('should setup header and body from ModalRef', () => {
    expect(fixture.debugElement.nativeElement.querySelector('.card-header > p > b').textContent).toBe('header');
    expect(fixture.debugElement.nativeElement.querySelector('.card-body > p').textContent).toBe('body');
  });

  it('should make ModalRef emits confirm string on confirm button press and detach modal', () => {
    fixture.debugElement.nativeElement.querySelector('.btn-primary-dark').click();
    expect(events$Spy).toHaveBeenCalledOnceWith('confirm');
    expect(detachSpy).toHaveBeenCalled();
  });

  it('should make ModalRef emits cancel string on cancel button press and detach modal', () => {
    fixture.debugElement.nativeElement.querySelector('.btn-danger').click();
    expect(events$Spy).toHaveBeenCalledOnceWith('cancel');
    expect(detachSpy).toHaveBeenCalled();
  });
});
