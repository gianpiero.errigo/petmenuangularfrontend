import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmModalComponent } from './confirm-modal.component';
import { ModalPresenterModule } from '../modal-presenter/modal-presenter.module';

@NgModule({
  declarations: [
    ConfirmModalComponent,
  ],
  imports: [
    CommonModule,
    ModalPresenterModule
  ],
  exports: [
    ConfirmModalComponent,
  ]
})
export class ConfirmModalModule { }
