import { Component, ElementRef, TemplateRef, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Positions } from 'src/app/model/modal-positions';
import { ModalService } from 'src/app/service/modal.service';

import { DropdownDirective } from './dropdown.directive';
import { DropdownModule } from './dropdown.module';

describe('DropdownDirective', () => {

  @Component({
    selector: 'app-dropdown-mock',
    template:
      `
		<button [appDropdown]='testTpl' position="top-left">Dropdown Trigger</button>
		<ng-template #testTpl>
			<div>Test</div>
		</ng-template>
		`
  })
  class DropdownMockComponent {
    @ViewChild(DropdownDirective, { read: ElementRef }) button!: ElementRef;
    @ViewChild('testTpl') testPanel!: TemplateRef<Element>;
  }

  let fixture: ComponentFixture<DropdownMockComponent>;
  let component: DropdownMockComponent;
  let modalServiceSpy: jasmine.SpyObj<ModalService>;

  beforeEach(() => {

    modalServiceSpy = jasmine.createSpyObj('ModalService', ['openDropdown']);

    TestBed.configureTestingModule({
      imports: [DropdownModule],
      declarations: [DropdownMockComponent],
      providers: [{ provide: ModalService, useValue: modalServiceSpy }]
    });

    fixture = TestBed.createComponent(DropdownMockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show dropdown on button click', () => {
    const mockedArgs = {
      origin: component.button,
      content: component.testPanel,
      positions: [Positions['top-left']]
    };
    fixture.debugElement.nativeElement.querySelector('button').click();
    expect(modalServiceSpy.openDropdown).toHaveBeenCalledOnceWith(mockedArgs);
  });

});
