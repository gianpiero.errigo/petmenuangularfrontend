import { Directive, ElementRef, HostListener, Input, TemplateRef } from '@angular/core';
import { Positions } from 'src/app/model/modal-positions';
import { ModalService } from 'src/app/service/modal.service';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  @Input('appDropdown') panelTpl!: TemplateRef<Element>;
  @Input() position: keyof typeof Positions = 'bottom-right';

  constructor(private origin: ElementRef,
    private modalService: ModalService) { }

  @HostListener('click', ['$event.target'])
  showDropdownPanel() {
    this.modalService.openDropdown({
      origin: this.origin,
      content: this.panelTpl,
      positions: [Positions[this.position]],
    });
  }

}
