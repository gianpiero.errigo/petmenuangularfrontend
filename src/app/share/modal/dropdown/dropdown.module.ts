import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownDirective } from './dropdown.directive';
import { ModalPresenterModule } from '../modal-presenter/modal-presenter.module';

@NgModule({
  declarations: [
    DropdownDirective,
  ],
  imports: [
    CommonModule,
    ModalPresenterModule
  ],
  exports: [
    DropdownDirective,
  ]
})
export class DropdownModule { }
