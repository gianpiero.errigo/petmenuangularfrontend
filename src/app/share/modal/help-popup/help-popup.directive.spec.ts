import { Component, ElementRef, TemplateRef, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Positions } from 'src/app/model/modal-positions';
import { ModalService } from 'src/app/service/modal.service';

import { HelpPopupDirective } from './help-popup.directive';
import { HelpPopupModule } from './help-popup.module';

describe('HelpPopupDirective', () => {

  @Component({
    selector: 'app-kelppopup-mock',
    template:
      `
		<button [appHelpPopup]='testTpl' position="top-left">Dropdown Trigger</button>
		<ng-template #testTpl>
			<div>Test</div>
		</ng-template>
		`
  })
  class HelpPopupMockComponent {
    @ViewChild(HelpPopupDirective, { read: ElementRef }) button!: ElementRef;
    @ViewChild('testTpl') testPanel!: TemplateRef<Element>;
  }

  let fixture: ComponentFixture<HelpPopupMockComponent>;
  let component: HelpPopupMockComponent;
  let modalServiceSpy: jasmine.SpyObj<ModalService>;

  beforeEach(() => {

    modalServiceSpy = jasmine.createSpyObj('ModalService', ['openHelpPopup']);

    TestBed.configureTestingModule({
      imports: [HelpPopupModule],
      declarations: [HelpPopupMockComponent],
      providers: [{ provide: ModalService, useValue: modalServiceSpy }]
    });

    fixture = TestBed.createComponent(HelpPopupMockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show dropdown on button click', () => {
    const mockedArgs = {
      origin: component.button,
      content: component.testPanel,
      positions: [Positions['top-left']]
    };
    fixture.debugElement.nativeElement.querySelector('button').click();
    expect(modalServiceSpy.openHelpPopup).toHaveBeenCalledOnceWith(mockedArgs);
  });

});
