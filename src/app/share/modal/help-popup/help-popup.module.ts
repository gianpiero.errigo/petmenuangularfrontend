import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpPopupDirective } from './help-popup.directive';
import { ModalPresenterModule } from '../modal-presenter/modal-presenter.module';

@NgModule({
  declarations: [
    HelpPopupDirective,
  ],
  imports: [
    CommonModule,
    ModalPresenterModule
  ],
  exports: [
    HelpPopupDirective,
  ]
})
export class HelpPopupModule { }
