import { Directive, ElementRef, HostListener, Input, TemplateRef } from '@angular/core';
import { Positions } from 'src/app/model/modal-positions';
import { ModalService } from 'src/app/service/modal.service';

@Directive({
  selector: '[appHelpPopup]'
})
export class HelpPopupDirective {

  @Input('appHelpPopup') panelTpl!: TemplateRef<Element>;
  @Input() position: keyof typeof Positions = 'bottom-left';

  constructor(private origin: ElementRef,
    private modalService: ModalService) { }

  @HostListener('click', ['$event.target'])
  showHelpPanel() {
    this.modalService.openHelpPopup({
      origin: this.origin,
      content: this.panelTpl,
      positions: [Positions[this.position]],
    });
  }

}
