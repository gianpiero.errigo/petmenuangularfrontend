import { OverlayRef } from '@angular/cdk/overlay';
import { ModalContent, ModalParams } from 'src/app/model/modal-params';
import { ReplaySubject } from 'rxjs';

export class ModalRef {

  events$: ReplaySubject<string> = new ReplaySubject();

  constructor(public overlay: OverlayRef,
    public content: ModalContent,
    public data: ModalParams) {

    if (overlay.getConfig().hasBackdrop && data.closingBackdrop) {
      overlay.backdropClick().subscribe(() => this.detach());
    }
    if (data.closingOutClick) {
      overlay.outsidePointerEvents().subscribe(() => this.detach());
    }
  }

  detach() {
    this.overlay.detach();
  }

}
