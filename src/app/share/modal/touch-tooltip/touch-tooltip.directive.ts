import { AfterViewInit, Directive, ElementRef, HostBinding } from '@angular/core';
import { ModalService } from 'src/app/service/modal.service';
import { Bottom, Top } from 'src/app/model/modal-positions';

@Directive({
  selector: '[appTouchTooltip]'
})
export class TouchTooltipDirective implements AfterViewInit {

  @HostBinding() className = 'd-inline-flex';
  observer!: ResizeObserver;

  constructor(private origin: ElementRef,
    private modalService: ModalService) { }

  ngAfterViewInit() {
    if (this.origin.nativeElement.parentNode.scrollWidth > this.origin.nativeElement.parentNode.clientWidth) {
      this.origin.nativeElement.addEventListener('touchstart', this.checkForMove);
    }
  }

  checkForMove = () => {
    let moved: boolean;
    this.origin.nativeElement.addEventListener('touchmove', () => { moved = true; });

    this.origin.nativeElement.addEventListener('touchend', (event: TouchEvent) => {
      if (moved !== true) {
        event.preventDefault();
        this.modalService.openTooltip({
          origin: this.origin,
          positions: [Bottom],
          content: this.origin.nativeElement.innerHTML,
        });
      }
    }, { once: true });
  };

}
