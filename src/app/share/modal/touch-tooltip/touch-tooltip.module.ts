import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TouchTooltipDirective } from './touch-tooltip.directive';
import { ModalPresenterModule } from '../modal-presenter/modal-presenter.module';

@NgModule({
  declarations: [
    TouchTooltipDirective,
  ],
  imports: [
    CommonModule,
    ModalPresenterModule
  ],
  exports: [
    TouchTooltipDirective,
  ]
})
export class TouchTooltipModule { }
