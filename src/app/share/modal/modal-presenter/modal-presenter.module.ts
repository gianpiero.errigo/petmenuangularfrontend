import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalPresenterComponent } from './modal-presenter.component';
import { OverlayModule } from '@angular/cdk/overlay';

@NgModule({
  declarations: [
    ModalPresenterComponent,
  ],
  imports: [
    CommonModule,
    OverlayModule
  ],
  exports: [
    ModalPresenterComponent,
  ]
})
export class ModalPresenterModule { }
