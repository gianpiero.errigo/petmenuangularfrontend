import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationStart, Router } from '@angular/router';
import { of } from 'rxjs';
import { ModalRef } from '../modal-ref';

import { ModalPresenterComponent } from './modal-presenter.component';
import { ModalPresenterModule } from './modal-presenter.module';

describe('ModalPresenterComponent', () => {
  let fixture: ComponentFixture<ModalPresenterComponent>;
  let routerSpy: jasmine.SpyObj<Router>;
  let modalRef: ModalRef;

  @Component({
    selector: 'app-modal-content-mock',
    template: `<div>Mock</div>`
  })
  class ModalContentMockComponent {}

  beforeEach(() => {

    routerSpy = jasmine.createSpyObj('Router', [], { events: of(new NavigationStart(0, '')) });
    const modalRefMock = { content: null, data: {}, events$: null, overlay: null, detach: () => null };

    TestBed.configureTestingModule({
      imports: [ModalPresenterModule],
      declarations: [ModalContentMockComponent],
      providers: [
        { provide: ModalRef, useValue: modalRefMock },
        { provide: Router, useValue: routerSpy }
      ],
    });



  });

  it('should render tooltip if content is of type text', () => {
    modalRef = TestBed.inject(ModalRef);
    modalRef.content = 'some text to show';
    fixture = TestBed.createComponent(ModalPresenterComponent);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('.pet-tooltip-panel')).toBeTruthy();
  });

  //	it('should show help if content is a template with animatroTpl set to help', () => {
  //		modalRefMock.content = new MockTemplateRef();
  //		modalRefMock.data.animatorTpl = 'help';
  //    fixture.detectChanges();
  //    expect(fixture.nativeElement.querySelector('#help')).toBeTruthy();
  //  });

  it('should render mocked component if content is of type component', () => {
    modalRef = TestBed.inject(ModalRef);
    modalRef.content = ModalContentMockComponent;
    fixture = TestBed.createComponent(ModalPresenterComponent);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('app-modal-content-mock')).toBeTruthy();
  });
});
