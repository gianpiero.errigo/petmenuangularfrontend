import { TemplateRef, OnInit, Component, HostBinding, OnDestroy, Type } from '@angular/core';
import { ModalRef } from '../modal-ref';
import { fade, triggerChildLeave, dropDown, scaleTR } from 'src/app/animations';
import { NavigationStart, Router } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { ModalParams } from 'src/app/model/modal-params';

@Component({
  selector: 'app-modal-presenter',
  templateUrl: './modal-presenter.component.html',
  animations: [triggerChildLeave, fade, dropDown, scaleTR]
})

export class ModalPresenterComponent implements OnInit, OnDestroy {

  @HostBinding('@triggerChildLeave') animateChild = true;
  @HostBinding() style = 'width:100%;word-wrap:break-word;';

  renderMethod: 'template' | 'component' | 'text' = 'component';
  context: {close: () => void, data: ModalParams} | null = null;

  componentContent?: Type<unknown>;
  templateContent: TemplateRef<Element> | null = null;
  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(public modalRef: ModalRef, private router: Router) {

    if (typeof this.modalRef.content === 'string') {
      this.renderMethod = 'text';
    }
    else if (this.modalRef.content instanceof TemplateRef) {
      this.renderMethod = 'template';
      this.templateContent = this.modalRef.content;
      this.context = {
        close: () => this.modalRef.detach(),
        data: this.modalRef.data
      };
    }
    else {
      this.componentContent = this.modalRef.content;
    }
  }

  ngOnInit() {
    this.router.events.pipe(
      takeUntil(this.destroy$),
      filter(event => event instanceof NavigationStart))
      .subscribe(() => this.modalRef.detach());
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
