import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoModalComponent } from './info-modal.component';
import { ModalPresenterModule } from '../modal-presenter/modal-presenter.module';

@NgModule({
  declarations: [
    InfoModalComponent,
  ],
  imports: [
    CommonModule,
    ModalPresenterModule
  ],
  exports: [
    InfoModalComponent,
  ]
})
export class InfoModalModule { }
