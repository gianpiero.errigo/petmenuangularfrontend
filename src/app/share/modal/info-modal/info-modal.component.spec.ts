import { OverlayRef } from '@angular/cdk/overlay';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReplaySubject } from 'rxjs';
import { ModalRef } from '../modal-ref';

import { InfoModalComponent } from './info-modal.component';
import { InfoModalModule } from './info-modal.module';

describe('InfoModalComponent', () => {
  let fixture: ComponentFixture<InfoModalComponent>;
  let modalRefMock: ModalRef;
  let overlayMock: OverlayRef;
  let events$Spy: jasmine.Spy;
  let detachSpy: jasmine.Spy;

  beforeEach(() => {

    modalRefMock = {
      content: '',
      data: { msg: { header: 'header', body: 'body' } },
      events$: new ReplaySubject<string>(),
      overlay: overlayMock, detach: () => null
    };
    events$Spy = spyOn(modalRefMock.events$, 'next');
    detachSpy = spyOn(modalRefMock, 'detach');


    TestBed.configureTestingModule({
      imports: [InfoModalModule],
      providers: [{ provide: ModalRef, useValue: modalRefMock }]
    });

    fixture = TestBed.createComponent(InfoModalComponent);
    fixture.detectChanges();
  });

  it('should setup header and body from ModalRef', () => {
    expect(fixture.debugElement.nativeElement.querySelector('.card-header > p > b').textContent).toBe('header');
    expect(fixture.debugElement.nativeElement.querySelector('.card-body > p').textContent).toBe('body');
  });

  it('should make ModalRef emits ok string on ok button press and detach modal', () => {
    fixture.debugElement.nativeElement.querySelector('.btn-primary-dark').click();
    expect(events$Spy).toHaveBeenCalledOnceWith('ok');
    expect(detachSpy).toHaveBeenCalled();
  });

});
