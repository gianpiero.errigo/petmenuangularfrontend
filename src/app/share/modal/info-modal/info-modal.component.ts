import { Component, OnInit } from '@angular/core';
import { ModalRef } from '../modal-ref';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.scss']
})
export class InfoModalComponent implements OnInit {

  header: string | undefined;
  body: string | undefined;
  constructor(private modalRef: ModalRef) { }

  ngOnInit(): void {
    this.header = this.modalRef.data.msg?.header;
    this.body = this.modalRef.data.msg?.body;
  }

  onClick() {
    this.modalRef.events$.next('ok');
    this.modalRef.detach();
  }

}
