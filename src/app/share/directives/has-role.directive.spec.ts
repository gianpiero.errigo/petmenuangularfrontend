import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthModule, AuthService } from '@auth0/auth0-angular';
import { of } from 'rxjs';
import { authConfig } from 'src/environments/environment';
import { HasRoleModule } from './has-role.module';

describe('HasRoleDirective', () => {

  @Component({
    selector: 'app-has-role-mock',
    template:
      `
    <p *appHasRole="['matching_role']">Test</p>
    `
  })
  class HasRoleMockComponent {}

  let fixture: ComponentFixture<HasRoleMockComponent>;
  let authService: AuthService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HasRoleModule,
        AuthModule.forRoot(authConfig)
      ],
      declarations: [HasRoleMockComponent],
    });

    fixture = TestBed.createComponent(HasRoleMockComponent);
    authService = TestBed.inject(AuthService);

  });

  it('should render view if role is present', () => {
    (authService as any).idTokenClaims$ = (of({ 'https://petmenu.eu/roles': ['different_role', 'matching_role'] }));
    fixture = TestBed.createComponent(HasRoleMockComponent);
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('p')).toBeTruthy();
  });

  it('should NOT render view if role is NOT present', () => {
    (authService as any).idTokenClaims$ = (of({ 'https://petmenu.eu/roles': ['different_role'] }));
    fixture = TestBed.createComponent(HasRoleMockComponent);
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('p')).toBeFalsy();
  });

});
