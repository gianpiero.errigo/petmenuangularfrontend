import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Directive({ selector: '[appHasRole]' })
export class HasRoleDirective {
  private hasView = false;
  private grantedRoles$: Observable<Array<string>> = this.auth.idTokenClaims$.pipe(
    filter(claims => !!claims),
    map(claims => claims?.['https://petmenu.eu/roles'])
  );

  constructor(
    private templateRef: TemplateRef<Element>,
    private viewContainer: ViewContainerRef,
    private auth: AuthService) { }

  @Input() set appHasRole(neededRoles: Array<string>) {
    this.grantedRoles$.subscribe(grantedRoles => {
      if (grantedRoles.some(granted => neededRoles.includes(granted) && !this.hasView)) {
        this.viewContainer.createEmbeddedView(this.templateRef);
        this.hasView = true;
      }
      else if (!grantedRoles.some(granted => neededRoles.includes(granted) && this.hasView)) {
        this.viewContainer.clear();
        this.hasView = false;
      }
    });
  }

}
