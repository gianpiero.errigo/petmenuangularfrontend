import { trigger, transition, style, animate, keyframes, animateChild, query, state, group } from '@angular/animations';

export const fromTopInAnimation = [
  style({ transform: 'translateY(-100%)' }),
  animate('300ms', style({ transform: 'translateY(0%)' }))
]

export const fromTopOutAnimation = [
  style({ transform: 'translateY(0%)' }),
  animate('300ms', style({ transform: 'translateY(-100%)' }))
]

export const fromTop =
  trigger('fromTop', [
    transition(':enter', fromTopInAnimation),
    transition(':leave', fromTopOutAnimation)
  ]);


export const fromBottomInAnimation = [
  style({ transform: 'translateY(100%)' }),
  animate('300ms', style({ transform: 'translateY(0%)' }))
]

export const fromBottomOutAnimation = [
  style({ transform: 'translateY(0%)' }),
  animate('300ms', style({ transform: 'translateY(100%)' }))
]

export const fromBottom =
  trigger('fromBottom', [
    transition(':enter', fromBottomInAnimation),
    transition(':leave', fromBottomOutAnimation)
  ]);


export const fromLeftInAnimation = [
  style({ transform: 'translateX(-100%)' }),
  animate('300ms', style({ transform: 'translateX(0%)' }))
]

export const fromLeftOutAnimation = [
  style({ transform: 'translateX(0%)' }),
  animate('300ms', style({ transform: 'translateX(-100%)' }))
]

export const fromLeft =
  trigger('fromLeft', [
    transition(':enter', fromLeftInAnimation),
    transition(':leave', fromLeftOutAnimation)
  ]);


export const fromRightInAnimation = [
  style({ transform: 'translateX(100%)' }),
  animate('300ms', style({ transform: 'translateX(0%)' })),
]

export const fromRightOutAnimation = [
  style({ transform: 'translateX(0%)' }),
  animate('300ms', style({ transform: 'translateX(100%)' }))
]

export const fromRight =
  trigger('fromRight', [
    transition(':enter', fromRightInAnimation),
    transition(':leave', fromRightOutAnimation)
  ]);


export const flipInAnimation = [
  style({ display: 'block' }),
  animate('300ms', keyframes([
    style({ transform: 'rotateX(-100deg)', 'transform-origin': 'top', opacity: 0, overflow: 'hidden' }),
    style({ transform: 'rotateX(0deg)', 'transform-origin': 'top', opacity: 1, overflow: 'visible' })
  ]))
]

export const flipOutAnimation = [
  style({ display: 'block' }),
  animate('300ms', keyframes([
    style({ transform: 'rotateX(0deg)', 'transform-origin': 'top', opacity: 1, overflow: 'hidden' }),
    style({ transform: 'rotateX(100deg)', 'transform-origin': 'top', opacity: 0, overflow: 'visible' })
  ]))
]

export const homeAnimationSequence =
  trigger('homeAnimationSequence', [
    transition('* => leaving', [
      query(':enter', style({ display: 'none' }), { optional: true }),
      group([
        query('.animate-flip', flipOutAnimation, { optional: true }),
        query('.animate-from-bottom', fromBottomOutAnimation, { optional: true })
      ])
    ]),
    transition('leaving => entering', [
      group([
        query('.animate-flip', flipInAnimation, { optional: true }),
        query('.animate-from-bottom', fromBottomInAnimation, { optional: true })
      ])
    ])
  ]);
  

export const growShrink =
  trigger('growShrink', [
    state('false', style({
      height: '0',
    })),
    state('true', style({
      height: '*',
    })),
    transition('false <=> true',
      group([
        query('*', animateChild()),
        animate('300ms')
      ])
    )
  ]);

export const triggerChildLeave =
  trigger('triggerChildLeave', [
    transition(':leave', [
      query(':leave', animateChild())
    ])
  ]);

export const scale =
  trigger('scale', [
    transition(':enter', [
      style({ transform: 'scale(0)' }),
      animate('300ms', style({ transform: 'scale(1)' }))
    ]),
    transition(':leave', [
      style({ transform: 'scale(1)' }),
      animate('300ms', style({ transform: 'scale(0)' }))
    ])
  ]);

export const scaleTR =
  trigger('scaleTR', [
    transition(':enter', [
      style({ transform: 'scale(0)', 'transform-origin': 'top right' }),
      animate('300ms ease-in-out', style({ transform: 'scale(1)' }))
    ]),
    transition(':leave', [
      style({ transform: 'scale(1)', 'transform-origin': 'top right' }),
      animate('300ms ease-in-out', style({ transform: 'scale(0)' }))
    ])
  ]);

export const fade =
  trigger('fade', [
    transition(':enter', [
      style({ opacity: 0 }),
      animate('150ms', style({ opacity: 1 }))
    ]),
    transition(':leave', [
      style({ opacity: 1 }),
      animate('150ms', style({ opacity: 0 }))
    ])
  ]);

export const puffRight =
  trigger('puffRight', [
    transition(':enter', [
      style({ transform: 'scale(2)', opacity: 0 }),
      animate('150ms', style({ transform: 'scale(1)', opacity: 1 }))
    ]),
    transition(':leave', [
      style({ opacity: 1 }),
      animate('150ms', style({ opacity: 0 }))
    ])
  ]);

export const dropDown =
  trigger('dropDown', [
    transition(':enter', [
      style({ transform: 'scaleY(0)', 'transform-origin': 'top center' }),
      animate('150ms', style({ transform: 'scaleY(1.1)' })),
      animate('50ms', style({ transform: 'scaleY(1)' }))
    ]),
    transition(':leave', [
      style({ transform: 'scaleY(1)', 'transform-origin': 'top center' }),
      animate('100ms', style({ transform: 'scaleY(0)' }))
    ]),
  ]);

  
export const paginator =
  trigger('paginator', [
    transition('* => leaveForNextPage', [
      style({ transform: 'translateX(0%)', opacity: 1 }),
      animate('300ms ease-in', style({ transform: 'translateX(-100%)', opacity: 0 }))
    ]),
    transition('* => leaveForPrevPage', [
      style({ transform: 'translateX(0%)', opacity: 1 }),
      animate('300ms ease-in', style({ transform: 'translateX(100%)', opacity: 0 }))
    ]),
    transition('* => enterNextPage', [
      style({ transform: 'translateX(100%)', opacity: 0 }),
      animate('300ms ease-out', style({ transform: 'translateX(0%)', opacity: 1 }))
    ]),
    transition('* => enterPrevPage', [
      style({ transform: 'translateX(-100%)', opacity: 0 }),
      animate('300ms ease-out', style({ transform: 'translateX(0%)', opacity: 1 }))
    ])
  ]);

export const selectedPress =
  trigger('selectedPress', [
    state('true', style({
      'box-shadow': '0 0 0.5rem var(--bs-tertiary-fluo)',
      'background-color': 'rgba(var(--bs-tertiary-light-rgb), .7)',
      'border-color': 'var(--bs-tertiary-fluo)'
    })),
    transition('false => true', [
      animate('300ms', keyframes([
        style({ 'box-shadow': 'none', offset: 0.5 }),
        style({
          'box-shadow': '0 0 0.5rem var(--bs-tertiary-fluo)',
          'background-color': 'rgba(var(--bs-tertiary-light-rgb), .7)',
          'border-color': 'var(--bs-tertiary-fluo)',
          offset: 1
        }),
      ])),
      query('.compare-check', animateChild())
    ]),
    transition('true => false', [
      animate('300ms'),
      query('.compare-check', animateChild())
    ])
  ]);

export const compButton =
  trigger('compButton', [
    transition(':enter', [
      style({ transform: 'translateX(100%)', 'border-color': 'var(--bs-secondary-fluo)' }),
      animate('300ms', style({ transform: 'translateX(0%)' })),
    ]),
    transition(':leave', [
      style({ transform: 'translateX(0%)' }),
      animate('300ms', style({ transform: 'translateX(100%)' }))
    ])
  ]);

export const wobbleVertRight =
  trigger('wobbleVertRight', [
    transition('false => true', [
      animate('0.7s', keyframes([
        style({ transform: 'translateY(0px) rotate(0deg)', offset: 0 }),
        style({ transform: 'translateY(-30px) rotate(6deg)', offset: 0.20 }),
        style({ transform: 'translateY(20px) rotate(-6deg)', offset: 0.30 }),
        style({ transform: 'translateY(-20px) rotate(4.5deg)', offset: 0.40 }),
        style({ transform: 'translateY(15px) rotate(-3.5deg)', offset: 0.50 }),
        style({ transform: 'translateY(-15px) rotate(3.5deg)', offset: 0.60 }),
        style({ transform: 'translateY(10px) rotate(-2deg)', offset: 0.70 }),
        style({ transform: 'translateY(-5px) rotate(1deg)', offset: 0.80 }),
        style({ transform: 'translateY(0px) rotate(0deg)', offset: 1 }),
      ]))
    ])
  ]);
  
  