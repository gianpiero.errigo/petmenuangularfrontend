export interface SSDto {
	frontImg: boolean;
	backImg: boolean;
	detailsOneImg: boolean;
	detailsTwoImg: boolean;
}

export const initSSDto = (): SSDto => (
  {
    frontImg: false,
    backImg: false,
    detailsOneImg: false,
    detailsTwoImg: false
  }
);
