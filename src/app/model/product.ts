import { FSDto } from './fsdto';
import { SSDto } from './ssdto';
import { TSDto } from './tsdto';

export interface Product {

	id: string;
  fsDto: FSDto;
  ssDto: SSDto;
	tsDto: TSDto;
  _stage: number;
}
