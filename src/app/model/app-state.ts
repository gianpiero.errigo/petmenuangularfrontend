import { locProdPackages, locProdTypes } from './enums';

export interface SearchParams {
	brand: string;
  line: string;
  prodType: keyof typeof locProdTypes;
  prodPackage: keyof typeof locProdPackages;
  recipe: string;
	ing: string;
	comp: string;
	minWeight: string;
	maxWeight: string;
	stage: string;
}

export const initSearchParams = (): SearchParams => (
  {
    brand: '',
    line: '',
    prodType: '',
    prodPackage: '',
    recipe: '',
    ing: '',
    comp: '',
    minWeight: '0',
    maxWeight: '0',
    stage: '',
  }
);
