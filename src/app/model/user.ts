export interface User {

	kcId: number;
	name: string;
	newCount: number;
	editCount: number;
	lastCommit: Date;
}

export interface UserDto {

	kcId: number;
	name: string;
	newCount: number;
	editCount: number;
	lastCommit: string;
}
