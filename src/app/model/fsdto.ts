import { locProdPackages, locProdTypes } from './enums';

export interface FSDto {

  brand: string;
  line: string;
  recipe: string;
  prodType: keyof typeof locProdTypes;
  prodPackage: keyof typeof locProdPackages;
	netWeight: number;
}

export const initFSDto = (): FSDto => (
  {
    brand: '',
    line: '',
    recipe: '',
    prodType: '',
    prodPackage: '',
    netWeight: 0,
  }
);
