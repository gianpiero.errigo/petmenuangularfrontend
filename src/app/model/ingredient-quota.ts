import { ComponentQuota } from './component-quota';

export interface IngredientQuota {

    ingredient: string;
    ingPerc?: number;
    compQuotasList: ComponentQuota[];
}
