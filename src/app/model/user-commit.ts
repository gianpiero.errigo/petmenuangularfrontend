export interface UserCommit {

	prodId: number;
	ucType: string;
	stage: number;
	time: Date;
}

export interface UserCommitDto {

	prodId: number;
	ucType: string;
	stage: number;
	time: string;
}
