import { Analysis } from './analysis';
import { IngredientQuota } from './ingredient-quota';

export interface TSDto {

	ingQuotasList: IngredientQuota[];
	analysis: Analysis;

}
