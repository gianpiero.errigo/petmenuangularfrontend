export interface ComponentQuota {

  component: string;
  compPerc?: number;
}
