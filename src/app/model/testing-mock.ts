import { Analysis } from './analysis';
import { ComponentQuota } from './component-quota';
import { FSDto } from './fsdto';
import { IngredientQuota } from './ingredient-quota';
import { Product } from './product';
import { SSDto } from './ssdto';
import { TSDto } from './tsdto';

export const fsDtoStub: FSDto = {
  brand: 'brand', line: 'line', recipe: 'recipe', prodType: 'DRY', prodPackage: 'BAG', netWeight: 1
};

export const ssDtoStub: SSDto = {
  frontImg: true, backImg: true, detailsOneImg: true, detailsTwoImg: true
};

export const compQuota1_A: ComponentQuota = {
  component: 'comp1_A',
  compPerc: 70,
}

export const compQuota1_B: ComponentQuota = {
  component: 'comp1_B',
  compPerc: undefined,
}

export const compQuota3_A: ComponentQuota = {
  component: 'comp3_A',
  compPerc: 80,
}

export const compQuota3_B: ComponentQuota = {
  component: 'comp3_B',
}

export const compQuota3_C: ComponentQuota = {
  component: 'comp3_C',
  compPerc: 30,
}

export const ingQuota1: IngredientQuota = {
  ingredient: 'ing1',
  ingPerc: 80,
  compQuotasList: [
    {...compQuota1_A},
    {...compQuota1_B}
  ]
}

export const ingQuota2: IngredientQuota = {
  ingredient: 'ing2',
  ingPerc: 10,
  compQuotasList: []
}

export const ingQuota3: IngredientQuota = {
  ingredient: 'ing3',
  compQuotasList: [
    {...compQuota3_A},
    {...compQuota3_B},
    {...compQuota3_C}
  ]
}

export const ingQuotasListStub: Array<IngredientQuota> = [
  {...ingQuota1},
  {...ingQuota2},
  {...ingQuota3}
]

export const analysisStub: Analysis = {
  rawProtein: 9,
  rawFibre: 8,
  rawOilFat: 7,
  rawAshes: 6,
  calcium: 5,
  sodium: 4,
  moisture: 60,
  energy: 99,
}

export const tsDtoStub: TSDto = {
  ingQuotasList: [...ingQuotasListStub],
  analysis: {...analysisStub}
};

export const productStub: Product = { id: '0', fsDto: {...fsDtoStub}, ssDto: {...ssDtoStub}, tsDto: {...tsDtoStub}, _stage: 3 };

