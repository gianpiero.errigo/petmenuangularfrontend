/* eslint-disable @typescript-eslint/naming-convention */

export interface Analysis {

	rawProtein: number;
	rawFibre: number;
	rawOilFat: number;
	rawAshes: number;
	calcium?: number;
	sodium?: number;
	phosphorus?: number;
	moisture: number;
	energy: number;
}

export const initAnalysis = (): Analysis => (
  {
    rawProtein: 0,
    rawFibre: 0,
    rawOilFat: 0,
    rawAshes: 0,
    moisture:0,
    calcium: 0,
    sodium: 0,
    phosphorus: 0,
    energy:0
  }
);

