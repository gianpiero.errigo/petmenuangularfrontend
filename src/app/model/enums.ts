/* eslint-disable @typescript-eslint/naming-convention */

import { Analysis } from './analysis';

export const locProdTypes = {
  '':  $localize`:opt@@anyProdType:--Any--`,
  DRY: $localize`:prodType@@dry:DRY`,
  WET: $localize`:prodType@@wet:WET`
} as const;

export const prodTypes = ['', 'DRY', 'WET'] as const;

export const isProdType = (input: string): input is keyof typeof locProdTypes => input in locProdTypes;


export const locProdPackages = {
  '':  $localize`:opt@@anyProdPackage:--Any--`,
  BAG: $localize`:prodPackage@@bag:BAG`,
  BOX: $localize`:prodPackage@@box:BOX`,
  CAN: $localize`:prodPackage@@can:CAN (&gt; 200gr.)`,
  POUCH: $localize`:prodPackage@@pouch:POUCH`,
  TRAY: $localize`:prodPackage@@tray:TRAY (&lt; 200gr.)`
} as const;

export const prodPackages = ['', 'BAG', 'BOX', 'CAN', 'POUCH', 'TRAY'] as const;

export const isProdPackage = (input: string): input is keyof typeof locProdPackages => input in locProdPackages;


export const locAvailableParamsKeys = {
  brand: $localize`:criteria@@brand:Brand`,
  line: $localize`:criteria@@line:Line`,
  recipe: $localize`:criteria@@recipe:Recipe`,
  ing: $localize`:criteria@@ingredient:Ingredient`,
  comp: $localize`:criteria@@component:Component`,
  prodType: $localize`:criteria@@prodType:Type`,
  prodPackage: $localize`:criteria@@prodPackage:Package`,
  stage: $localize`:criteria@@stage:Stage`,
  minWeight: $localize`:criteria@@minWeight:Min Weight`,
  maxWeight: $localize`:criteria@@maxWeight:Max Weight`
} as const;

export const availableParamsKeys = [
  'brand', 'line', 'recipe', 'ing', 'comp', 'prodType', 'prodPackage', 'stage', 'minWeight', 'maxWeight'
] as const;

export const isAvailableParamsKey = (input: string): input is keyof typeof locAvailableParamsKeys => input in locAvailableParamsKeys;


export const initLocAnalysis = (): Record<keyof Analysis, {display: string; value: number}> => (
  {
    rawProtein: {display: $localize`:@@protein:Protein`, value: 0},
    rawFibre: {display: $localize`:@@fibre:Fibre`, value: 0},
    rawOilFat: { display: $localize`:@@oilfat:Oil and Fat`, value: 0},
    rawAshes: {display: $localize`:@@ashes:Ashes`, value: 0},
    calcium: {display: $localize`:@@calcium:Calcium`, value: 0},
    sodium: {display: $localize`:@@sodium:Sodium`, value: 0},
    phosphorus: {display: $localize`:@@phosphorus:Phosphorus`, value: 0},
    moisture: {display: $localize`:@@moisture:Moisture`, value: 0},
    energy: {display: $localize`:@@energy:Energy`, value: 0}
  } as const
);

export const analysisKeys = [
  'rawProtein', 'rawFibre', 'rawOilFat', 'rawAshes', 'calcium', 'sodium', 'phosphorus', 'moisture', 'energy'
] as const;

