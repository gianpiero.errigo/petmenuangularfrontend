import { ConnectionPositionPair } from '@angular/cdk/overlay';
import { TemplateRef, Type, ElementRef } from '@angular/core';

export type ModalContent = TemplateRef<Element> | Type<unknown> | string;
export type ModalMsg = { header: string; body?: string; footer?: string };
export type ModalParams = {
	content?: ModalContent;
	origin?: ElementRef | Element | {x: number; y: number};
	positions?: ConnectionPositionPair[];
	offsetX?: number;
	offsetY?: number;
	animatorTpl?: string;
	hasBackdrop?: boolean;
	closingBackdrop?: boolean;
	backdropClass?: string;
	closingOutClick?: boolean;
	msg?: ModalMsg;
};
