/* eslint-disable @typescript-eslint/naming-convention */
import { ConnectionPositionPair } from '@angular/cdk/overlay';

export const Top: ConnectionPositionPair = {
  originX : 'center',
  originY : 'top',
  overlayX: 'center',
  overlayY: 'bottom',
} as const;

export const Bottom: ConnectionPositionPair = {
  originX : 'center',
  originY : 'bottom',
  overlayX: 'center',
  overlayY: 'top',
} as const;

export const Left: ConnectionPositionPair = {
  originX : 'start',
  originY : 'center',
  overlayX: 'end',
  overlayY: 'center',
} as const;

export const TopRight: ConnectionPositionPair = {
  originX : 'center',
  originY : 'top',
  overlayX: 'start',
  overlayY: 'bottom',
} as const;

export const TopLeft: ConnectionPositionPair = {
  originX : 'center',
  originY : 'top',
  overlayX: 'end',
  overlayY: 'bottom',
} as const;

export const BottomRight: ConnectionPositionPair = {
  originX : 'center',
  originY : 'bottom',
  overlayX: 'start',
  overlayY: 'top',
} as const;

export const BottomLeft: ConnectionPositionPair = {
  originX : 'center',
  originY : 'bottom',
  overlayX: 'end',
  overlayY: 'top',
} as const;

export const Positions = {
  'top-right': TopRight,
  'top-left': TopLeft,
  'bottom-right': BottomRight,
  'bottom-left': BottomLeft,
  bottom: Bottom,
  left: Left,
} as const;
