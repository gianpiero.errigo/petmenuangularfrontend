import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Store } from '@ngrx/store';
import { availableParamsKeys } from 'src/app/model/enums';
import { showProduct } from 'src/app/ngrx/product-view/product-view.actions';

@Injectable({ providedIn: 'root' })
export class ProductDetailViewResolver implements Resolve<void> {

  availableParamsKeys = [...availableParamsKeys];

  constructor(private readonly appState: Store) { }

  resolve(route: ActivatedRouteSnapshot): void {
    const idQueryParam = route.queryParamMap.get('id');
    if (idQueryParam) {
      this.appState.dispatch(showProduct({ shownProdId: idQueryParam }));
    }
  }

}
