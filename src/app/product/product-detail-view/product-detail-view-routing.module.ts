import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductDetailViewComponent } from './product-detail-view.component';
import { ProductDetailViewResolver } from './product-detail-view.resolver';

const routes: Routes = [

  {
    path: '',
    component: ProductDetailViewComponent,
    resolve: {_: ProductDetailViewResolver},
    runGuardsAndResolvers: 'paramsOrQueryParamsChange'
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductDetailViewRoutingModule {}
