import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EMPTY, of } from 'rxjs';
import { productStub } from 'src/app/model/testing-mock';
import { getComparedProducts } from 'src/app/ngrx/compare/compare.selectors';
import { getShownProduct } from 'src/app/ngrx/product-view/product-view.selectors';
import { ProdService } from 'src/app/service/prod.service';
import { ProductDetailViewComponent } from './product-detail-view.component';
import { ProductDetailViewModule } from './product-detail-view.module';


describe('ProductDetailViewComponent', () => {
  let component: ProductDetailViewComponent;
  let fixture: ComponentFixture<ProductDetailViewComponent>;
  let store: MockStore;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ProductDetailViewModule],
      providers: [
        provideMockStore({
          initialState: {
            productView: {
              showProduct: { shownProdId: '0', shownProduct: undefined }
            },
            compare: { products: [] }
          }
        }),
        { provide: ProdService, useValue: {} },
        { provide: Router, useValue: {} },
        { provide: AuthService, useValue: {} },
        {
          provide: ActivatedRoute,
          useValue: {
            queryParamMap: of({ get: (param: string) => param === 'id' ? '0' : null}),
            queryParams: EMPTY
          }
        },
      ]
    });

    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(ProductDetailViewComponent);
    component = fixture.componentInstance;
  });
         
  it('should show only first stage component if _stage is 1', (done) => {
    store.overrideSelector(getShownProduct, { ...productStub, _stage: 1 });
    store.overrideSelector(getComparedProducts, []);

    store.refreshState();
    fixture.detectChanges();

    component.vm$.subscribe(() => { 
      expect(fixture.debugElement.query(By.css('app-first-stage-view'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('app-second-stage-view'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('app-third-stage-view'))).toBeFalsy();
      done();
    });
  });

  it('should show only first 2 stage components if _stage is 2', (done) => {
    store.overrideSelector(getShownProduct, { ...productStub, _stage: 2 });
    store.overrideSelector(getComparedProducts, []);

    store.refreshState();
    fixture.detectChanges();

    component.vm$.subscribe(() => {
      expect(fixture.debugElement.query(By.css('app-first-stage-view'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('app-second-stage-view'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('app-third-stage-view'))).toBeFalsy();
      done();
    });
  });

  it('should show all 3 stage components if _stage is 3', (done) => {
    store.overrideSelector(getShownProduct, { ...productStub, _stage: 3 });
    store.overrideSelector(getComparedProducts, []);

    store.refreshState();
    fixture.detectChanges();

    component.vm$.subscribe(() => {
      expect(fixture.debugElement.query(By.css('app-first-stage-view'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('app-second-stage-view'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('app-third-stage-view'))).toBeTruthy();
      done();
    });
  });

  it('should highlight and show stamp if product is selected for comparison', waitForAsync(() => {
    store.overrideSelector(getShownProduct, { ...productStub, _stage: 3 });
    store.overrideSelector(getComparedProducts, [{...productStub}]);
  
    store.refreshState();
    fixture.detectChanges();

    fixture.whenRenderingDone().then(() => {
      const stamp = fixture.debugElement.query(By.css('div > .row > .col > div:nth-child(2) > div > div > span'));
      const highlightEl = fixture.debugElement.query(By.css('.transient-prod-view'));
      expect(stamp.nativeElement.textContent).toContain('Checked for comparison');
      expect(highlightEl.nativeElement.style.backgroundColor).toBe('rgba(var(--bs-tertiary-light-rgb), 0.7)');
    });
  }));

});
