import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest } from 'rxjs';
import { Product } from 'src/app/model/product';
import { filter, map } from 'rxjs/operators';
import { fromTop, puffRight, selectedPress } from 'src/app/animations';
import { faCheckDouble } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { getShownProduct } from 'src/app/ngrx/product-view/product-view.selectors';
import { toggleCheckedForCompare } from 'src/app/ngrx/compare/compare.actions';
import { getComparedProducts } from 'src/app/ngrx/compare/compare.selectors';

@Component({
  selector: 'app-product-detail-view',
  templateUrl: './product-detail-view.component.html',
  styleUrls: ['./product-detail-view.component.scss'],
  animations: [fromTop, selectedPress, puffRight]
})

export class ProductDetailViewComponent {
  isCheckedForCompare$ = combineLatest([
    this.route.queryParamMap.pipe(
      map(paramMap => paramMap.get('id')),
      filter((id): id is string => !!id)
    ),
    this.appState.select(getComparedProducts)
  ]).pipe(
    map(([id, comparedProducts]) => comparedProducts.some(compared => compared.id === id))
  );

  product$ = this.appState.select(getShownProduct).pipe(filter((product): product is Product => !!product));

  vm$ = combineLatest({product: this.product$, checkedForCompare: this.isCheckedForCompare$})

  iconChecked = faCheckDouble;

  constructor(
    private route: ActivatedRoute,
    private appState: Store,
  ) { }

  listenToCompare(product: Product, posX: number, posY: number) {
    this.appState.dispatch(toggleCheckedForCompare({ product, posX, posY }));
  }

}
