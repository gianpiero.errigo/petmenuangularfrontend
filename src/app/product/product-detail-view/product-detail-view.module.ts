import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FirstStageViewModule } from '../stage-views/first-stage-view/first-stage-view.module';
import { SecondStageViewModule } from '../stage-views/second-stage-view/second-stage-view.module';
import { ThirdStageViewModule } from '../stage-views/third-stage-view/third-stage-view.module';
import { ProductDetailViewRoutingModule } from './product-detail-view-routing.module';
import { ProductDetailViewComponent } from './product-detail-view.component';


@NgModule({
  declarations:[
    ProductDetailViewComponent,
  ],
  imports: [
    CommonModule,
    ProductDetailViewRoutingModule,
    FontAwesomeModule,
    FirstStageViewModule,
    SecondStageViewModule,
    ThirdStageViewModule
  ],
  exports: [
    ProductDetailViewComponent
  ]
})
export class ProductDetailViewModule { }
