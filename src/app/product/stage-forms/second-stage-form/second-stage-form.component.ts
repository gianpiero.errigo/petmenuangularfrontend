import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SSDto } from 'src/app/model/ssdto';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { environment } from 'src/environments/environment';
import { scaleTR } from 'src/app/animations';
import { Store } from '@ngrx/store';
import { submitEditSecondStage, submitNewSecondStage } from 'src/app/ngrx/product-edit/product-edit.actions';

@Component({
  selector: 'app-second-stage-form',
  templateUrl: './second-stage-form.component.html',
  styleUrls: ['./second-stage-form.component.scss'],
  animations: [scaleTR]
})
export class SecondStageFormComponent {

  @Input() editType = '';
  @Input() id = '';

  @Input()
  set ssDto(ssDto: SSDto) {
    if (ssDto) {
      this.prePopulateThumbs(ssDto);
    }
  }

  imagesForm: FormGroup;

  imgsDir = environment.pmImgs;
  imgsSrc = {
    frontImg: 'assets/imgs/no_img_200.jpg',
    backImg: 'assets/imgs/no_img_200.jpg',
    detailsOneImg: 'assets/imgs/no_img_200.jpg',
    detailsTwoImg: 'assets/imgs/no_img_200.jpg',
  };

  iconHelp = faQuestionCircle;

  constructor(private appStore: Store,
    private formBuilder: FormBuilder) {
    this.imagesForm = this.formBuilder.group({
      frontImg: [null, Validators.required],
      backImg: [null, Validators.required],
      detailsOneImg: [null, Validators.required],
      detailsTwoImg: [null, Validators.required]
    });
  }

  prePopulateThumbs(_ssDto: SSDto) {
    if (_ssDto.frontImg) {
      this.imgsSrc.frontImg = `${this.imgsDir}/${this.id}/frontImg_sm.jpg?${Date.now()}`;
    }
    if (_ssDto.backImg) {
      this.imgsSrc.backImg = `${this.imgsDir}/${this.id}/backImg_sm.jpg?${Date.now()}`;
    }
    if (_ssDto.detailsOneImg) {
      this.imgsSrc.detailsOneImg = `${this.imgsDir}/${this.id}/detailsOneImg_sm.jpg?${Date.now()}`;
    }
    if (_ssDto.detailsTwoImg) {
      this.imgsSrc.detailsTwoImg = `${this.imgsDir}/${this.id}/detailsTwoImg_sm.jpg?${Date.now()}`;
    }
  }

  onImgSelect(event: Event, slot: keyof SSDto) {
    const target = event.target as HTMLInputElement;
    if (target.files?.length && target.files?.length > 0) {
      const file = target.files?.[0];
      this.imagesForm.get(slot)?.setValue(file);

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (reader.result) {
          this.imgsSrc[slot] = reader.result.toString();
        }
      };
    }
  }

  onSubmit() {

    const imagesData = new FormData();

    Object.keys(this.imagesForm.controls).forEach((key: string) => {
      imagesData.append(key, this.imagesForm.get(key)?.value);
    });

    if (this.editType === 'MODIFY') {
      this.appStore.dispatch(submitEditSecondStage({ id: this.id, formValue: imagesData }));
    }
    else if (this.editType === 'NEW') {
      this.appStore.dispatch(submitNewSecondStage({ id: this.id, formValue: imagesData }));
    }
  }
}

