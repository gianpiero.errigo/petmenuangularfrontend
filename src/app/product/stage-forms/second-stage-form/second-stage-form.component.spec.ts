import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { submitEditSecondStage, submitNewSecondStage } from 'src/app/ngrx/product-edit/product-edit.actions';
import { environment } from 'src/environments/environment';

import { SecondStageFormComponent } from './second-stage-form.component';
import { SecondStageFormModule } from './second-stage-form.module';

describe('SecondStageFormComponent', () => {
  let component: SecondStageFormComponent;
  let fixture: ComponentFixture<SecondStageFormComponent>;
  let store: MockStore;
  let frontImgDataMock: File;
  let backImgDataMock: File;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SecondStageFormModule, RouterTestingModule],
      providers: [
        provideMockStore({
          initialState: {
            productEdit: {
              prodEditId: '', prodEditType: '', prodEditStage: undefined
            }
          }
        }),
      ]
    });
    environment.pmImgs = 'mockBackend/stubImgsDir';
    fixture = TestBed.createComponent(SecondStageFormComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);

    spyOn(store, 'dispatch');
    component.id = '0';
    fixture.detectChanges();
  });

  it('should pre-populate images slot if modifying existing product', () => {
    component.editType = 'MODIFY';
    component.ssDto = { frontImg: true, backImg: true, detailsOneImg: false, detailsTwoImg: false };
    fixture.detectChanges();
    expect(component.imgsSrc.frontImg).toContain(`mockBackend/stubImgsDir/0/frontImg_sm.jpg`);
    expect(component.imgsSrc.backImg).toContain(`mockBackend/stubImgsDir/0/backImg_sm.jpg`);
    expect(component.imgsSrc.detailsOneImg).toBe(`assets/imgs/no_img_200.jpg`);
    expect(component.imgsSrc.detailsTwoImg).toBe(`assets/imgs/no_img_200.jpg`);
  });

  it('should set form control and thumbnail source for correct slot on file selection', () => {
    const mockFileReader = jasmine.createSpyObj<FileReader>('FileReader', ['readAsDataURL'], { result: 'mockData' });
    const mockImg = new File(['mock'], 'mock.png');
    const mockEvent = { target: { files: [mockImg] } } as unknown as Event;
    mockFileReader.onload = () => ({});
    globalThis.FileReader = function() {
      return mockFileReader;
    } as unknown as typeof FileReader;

    component.onImgSelect(mockEvent, 'frontImg');
    mockFileReader.onload({} as ProgressEvent<FileReader>);
    expect(component.imgsSrc.frontImg).toBe('mockData');
    expect(component.imagesForm.get('frontImg')?.value).toBe(mockImg);
  });

  describe('based on editType', () => {
    beforeEach(() => {
      frontImgDataMock = new File(['mock'], 'mockFront.png');
      backImgDataMock = new File(['mock'], 'mockBack.png');
      component.imagesForm.setValue({
        frontImg: frontImgDataMock,
        backImg: backImgDataMock,
        detailsOneImg: null,
        detailsTwoImg: null
      });
    });

    it('should call submitEditStage action if editType is MODIFY', () => {
      component.editType = 'MODIFY';
      fixture.debugElement.query(By.css('button[type="submit"]')).nativeElement.click();
      expect(store.dispatch).toHaveBeenCalledWith(
        submitEditSecondStage({
          id: '0', formValue: new FormData()
        })
      );
    });

    it('should call submitNewtStage action if editType is NEW', () => {
      component.editType = 'NEW';
      fixture.debugElement.query(By.css('button[type="submit"]')).nativeElement.click();
      expect(store.dispatch).toHaveBeenCalledWith(
        submitNewSecondStage({
          id: '0', formValue: new FormData()
        })
      );
    });

  });

});
