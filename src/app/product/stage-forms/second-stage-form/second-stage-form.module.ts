import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SecondStageFormComponent } from './second-stage-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HelpPopupModule } from 'src/app/share/modal/help-popup/help-popup.module';


@NgModule({
  declarations: [
    SecondStageFormComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    HelpPopupModule
  ],
  exports: [
    SecondStageFormComponent
  ]
})
export class SecondStageFormModule { }
