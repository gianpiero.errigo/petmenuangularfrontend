import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReplaySubject, BehaviorSubject, iif, of, Observable } from 'rxjs';
import { takeUntil, filter, distinctUntilChanged, switchMap, map, debounceTime, tap } from 'rxjs/operators';
import { FSDto } from '../../../model/fsdto';
import { LineService } from '../../../service/line.service';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { BrandService } from '../../../service/brand.service';
import { ProdService } from '../../../service/prod.service';
import { prodTypes, locProdTypes, prodPackages, locProdPackages } from '../../../model/enums';
import { scaleTR } from 'src/app/animations';
import { Store } from '@ngrx/store';
import { editProduct, resetEditState, submitEditFirstStage, submitNewFirstStage } from 'src/app/ngrx/product-edit/product-edit.actions';
import { getProductEditType } from 'src/app/ngrx/product-edit/product-edit.selectors';


@Component({
  selector: 'app-first-stage-form',
  templateUrl: './first-stage-form.component.html',
  styleUrls: ['./first-stage-form.component.css'],
  animations: [scaleTR]
})
export class FirstStageFormComponent implements OnInit, OnDestroy {

  @Input() id = '';

  prodTypes = [...prodTypes];
  locProdTypes = { ...locProdTypes };
  prodPackages = [...prodPackages];
  locProdPackages = { ...locProdPackages };

  @Input()
  set fsDto(_fsDto: FSDto) {
    this.fsDto$.next(_fsDto);
  }

  fsForm: FormGroup;

  iconHelp = faQuestionCircle;

  #suggestedLines: string[] = [];
  #suggestedBrands: string[] = [];
  #suggestedRecipes: string[] = [];

  editType$ = this.appStore.select(getProductEditType);
  private fsDto$ = new BehaviorSubject<FSDto | null>(null);

  private destroy$: ReplaySubject<boolean> = new ReplaySubject(1);


  constructor(private router: Router,
    private appStore: Store,
    private brandService: BrandService,
    private lineService: LineService,
    private prodService: ProdService,
    private formBuilder: FormBuilder) {

    this.fsForm = this.formBuilder.group({
      brand: ['', { validators: Validators.required, updateOn: 'blur' }],
      line: ['', { validators: Validators.required, updateOn: 'blur' }],
      recipe: ['', Validators.required],
      prodType: [null, Validators.required],
      prodPackage: [null, Validators.required],
      netWeight: [null, { validators: [Validators.required, Validators.min(1)] }]
    });
  }

  ngOnInit() {

    if (this.router.url.endsWith('product/edit/new')) {
      this.appStore.dispatch(editProduct({ prodEditId: '', prodEditType: 'NEW', prodEditStage: 1 }));
    }

    this.fsDto$.pipe(
      takeUntil(this.destroy$),
      filter((fsDto): fsDto is FSDto => !!fsDto))
      .subscribe(fsDto => this.fsForm.setValue(fsDto));

    this.brandService.get().pipe(
      takeUntil(this.destroy$))
      .subscribe(_brands => this.#suggestedBrands = _brands);

    this.fsForm.get('brand')?.valueChanges.pipe(
      takeUntil(this.destroy$),
      filter(_brand => !!_brand),
      distinctUntilChanged(),
      switchMap((_brand: string) => this.lineService.getFromBrand(_brand)))
      .subscribe(lines => this.#suggestedLines = lines);

    this.fsForm.get('line')?.valueChanges.pipe(
      takeUntil(this.destroy$),
      filter(_line => !!_line),
      distinctUntilChanged(),
      switchMap((_line: string) => this.prodService.getRecipesByLine(this.fsForm.get('brand')?.value, _line)),
      map((withDuplicates) => [...new Set(withDuplicates)]))
      .subscribe(recipesNoDup => this.#suggestedRecipes = recipesNoDup);

  }

  searchBrands = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.#suggestedBrands.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  searchLines = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.#suggestedLines.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  searchRecipes = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.#suggestedRecipes.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  onSubmit() {
    this.editType$.pipe(
      takeUntil(this.destroy$))
      .subscribe(editType => {
        if (editType === 'MODIFY') {
          this.appStore.dispatch(submitEditFirstStage({ id: this.id, formValue: this.fsForm.value }));
        }
        else if (editType === 'NEW') {
          this.appStore.dispatch(submitNewFirstStage({ formValue: this.fsForm.value }));
        }
      });
  }

  onCancel() {
    this.editType$.pipe(
      takeUntil(this.destroy$),
      switchMap((editType) => iif(() => editType === 'NEW', of('/home'), of(`product/view?id=${this.id}`))),
      tap(() => this.appStore.dispatch(resetEditState())))
      .subscribe((destination: string) => this.router.navigateByUrl(destination));
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
