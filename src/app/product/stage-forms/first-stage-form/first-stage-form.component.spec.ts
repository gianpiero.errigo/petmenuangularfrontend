import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { editProduct, resetEditState, submitEditFirstStage, submitNewFirstStage } from 'src/app/ngrx/product-edit/product-edit.actions';
import { BrandService } from 'src/app/service/brand.service';
import { LineService } from 'src/app/service/line.service';
import { ProdService } from 'src/app/service/prod.service';
import { getTestScheduler, cold } from 'jasmine-marbles';
import { fsDtoStub } from 'src/app/model/testing-mock';
import { FirstStageFormComponent } from './first-stage-form.component';
import { getProductEditType } from 'src/app/ngrx/product-edit/product-edit.selectors';
import { By } from '@angular/platform-browser';
import { FirstStageFormModule } from './first-stage-form.module';


describe('FirstStageFormComponent', () => {
  let component: FirstStageFormComponent;
  let fixture: ComponentFixture<FirstStageFormComponent>;
  let routerSpy: jasmine.SpyObj<Router>;
  let brandSpy: jasmine.SpyObj<BrandService>;
  let lineSpy: jasmine.SpyObj<LineService>;
  let recipeSpy: jasmine.SpyObj<ProdService>;

  let store: MockStore;

  beforeEach(() => {
    routerSpy = jasmine.createSpyObj('Router', ['navigate', 'navigateByUrl'], { url: 'mock/product/edit/new' });
    brandSpy = jasmine.createSpyObj('BrandService', ['get']);
    lineSpy = jasmine.createSpyObj('LineService', ['getFromBrand']);
    recipeSpy = jasmine.createSpyObj('ProdService', ['getRecipesByLine']);

    TestBed.configureTestingModule({
      imports: [FirstStageFormModule],
      providers: [
        { provide: Router, useValue: routerSpy },
        provideMockStore({
          initialState: {
            productEdit: {
              prodEditId: '', prodEditType: '', prodEditStage: undefined
            }
          }
        }),
        { provide: BrandService, useValue: brandSpy },
        { provide: LineService, useValue: lineSpy },
        { provide: ProdService, useValue: recipeSpy },
      ],
    });

    fixture = TestBed.createComponent(FirstStageFormComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);

    spyOn(store, 'dispatch');
    brandSpy.get.and.returnValue(of(['abc', 'cde']));
    lineSpy.getFromBrand.and.returnValue(of(['abc', 'cde']));
    recipeSpy.getRecipesByLine.and.returnValue(of(['abc', 'cde', 'cde']));

    fixture.detectChanges();

  });

  it('should dispatch "NEW" edit type if inserting a new product', () => {
    expect(store.dispatch).toHaveBeenCalledWith(editProduct({ prodEditId: '', prodEditType: 'NEW', prodEditStage: 1 }));
  });

  describe('populate typeahead variables', () => {

    it('should suggest matching brands on partial input', () => {
      getTestScheduler().run(helpers => {
        const { expectObservable } = helpers;
        const userInput$ = cold('a 300ms b 100ms a 200ms b', { a: 'c', b: 'cd' });
        expectObservable(component.searchBrands(userInput$)).toBe('200ms a 401ms a 200ms b', { a: ['abc', 'cde'], b: ['cde'] });
      });

    });

    it('should suggest matching lines on partial input', () => {
      component.fsForm.get('brand')?.setValue('Trigger stub Brand value change');
      getTestScheduler().run(helpers => {
        const { expectObservable } = helpers;
        const userInput$ = cold('a 300ms b 100ms a 200ms b', { a: 'c', b: 'cd' });
        expectObservable(component.searchLines(userInput$)).toBe('200ms a 401ms a 200ms b', { a: ['abc', 'cde'], b: ['cde'] });
      });

    });

    it('should suggest matching recipes on partial input', () => {
      component.fsForm.get('line')?.setValue('Trigger stub Line value change');
      getTestScheduler().run(helpers => {
        const { expectObservable } = helpers;
        const userInput$ = cold('a 300ms b 100ms a 200ms b', { a: 'c', b: 'cd' });
        expectObservable(component.searchRecipes(userInput$)).toBe('200ms a 401ms a 200ms b', { a: ['abc', 'cde'], b: ['cde'] });
      });

    });

  });

  describe('submission', () => {

    describe('button', () => {

      it('should be disabled when form is invalid', () => {
        component.fsForm.markAsDirty();
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('button[type="submit"]')).nativeElement.disabled).toBeTruthy();
      });

      it('should be disabled when form is pristine', () => {
        component.fsForm.setValue(fsDtoStub);
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('button[type="submit"]')).nativeElement.disabled).toBeTruthy();
      });

      it('should be enabled only when form is valid and dirty', () => {
        component.fsForm.setValue(fsDtoStub);
        component.fsForm.markAsDirty();
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('button[type="submit"]')).nativeElement.disabled).toBeFalsy();
      });

    });

    it('should dispatch action for new product', () => {
      store.overrideSelector(getProductEditType, 'NEW');
      component.onSubmit();
      expect(store.dispatch).toHaveBeenCalledWith(submitNewFirstStage({ formValue: component.fsForm.value }));
    });

    it('should dispatch action for editing existing product', () => {
      store.overrideSelector(getProductEditType, 'MODIFY');
      component.onSubmit();
      expect(store.dispatch).toHaveBeenCalledWith(submitEditFirstStage({ id: component.id, formValue: component.fsForm.value }));
    });

  });

  describe('on cancel after dispatch action to reset editing state', () => {

    it('should redirect to "/home" for new product', () => {
      store.overrideSelector(getProductEditType, 'NEW');
      component.onCancel();
      expect(store.dispatch).toHaveBeenCalledWith(resetEditState());
      expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/home');
    });

    it('should redirect to view for editing existing product', () => {
      store.overrideSelector(getProductEditType, 'MODIFY');
      component.onCancel();
      expect(store.dispatch).toHaveBeenCalledWith(resetEditState());
      expect(routerSpy.navigateByUrl).toHaveBeenCalledWith(`product/view?id=${component.id}`);
    });

  });
});
