import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FirstStageFormComponent } from './first-stage-form.component';
import { HelpPopupModule } from 'src/app/share/modal/help-popup/help-popup.module';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    FirstStageFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DragDropModule,
    FontAwesomeModule,
    NgbTypeaheadModule,
    HelpPopupModule,
  ],
  exports: [
    FirstStageFormComponent
  ]
})
export class FirstStageFormModule { }
