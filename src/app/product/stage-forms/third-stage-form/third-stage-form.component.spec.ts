import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold, getTestScheduler } from 'jasmine-marbles';
import { of } from 'rxjs';
import { tsDtoStub } from 'src/app/model/testing-mock';
import { submitEditThirdStage, submitNewThirdStage } from 'src/app/ngrx/product-edit/product-edit.actions';
import { ComponentService } from 'src/app/service/component.service';
import { IngredientService } from 'src/app/service/ingredient.service';

import { ThirdStageFormComponent } from './third-stage-form.component';
import { ThirdStageFormModule } from './third-stage-form.module';

describe('ThirdStageFormComponent', () => {
  let component: ThirdStageFormComponent;
  let fixture: ComponentFixture<ThirdStageFormComponent>;
  let store: MockStore;
  let ingredientSpy: jasmine.SpyObj<IngredientService>;
  let componentSpy: jasmine.SpyObj<ComponentService>;

  beforeEach(() => {
    ingredientSpy = jasmine.createSpyObj('IngredientService', ['get']);
    componentSpy = jasmine.createSpyObj('ComponentService', ['get']);

    TestBed.configureTestingModule({
      imports: [ThirdStageFormModule, RouterTestingModule],
      providers: [
        provideMockStore({
          initialState: {
            productEdit: {
              prodEditId: '', prodEditType: '', prodEditStage: undefined
            }
          }
        }),
        { provide: IngredientService, useValue: ingredientSpy },
        { provide: ComponentService, useValue: componentSpy }
      ]
    });

    fixture = TestBed.createComponent(ThirdStageFormComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);

    spyOn(store, 'dispatch');
    ingredientSpy.get.and.returnValue(of(['abc', 'cde']));
    componentSpy.get.and.returnValue(of(['abc', 'cde']));

  });

  describe('populate typeahead variables', () => {

    it('should suggest matching ingredients on partial input', () => {
      fixture.detectChanges();
      getTestScheduler().run(helpers => {
        const { expectObservable } = helpers;
        const userInput$ = cold('a 300ms b 100ms a 200ms b', { a: 'c', b: 'cd' });
        expectObservable(component.searchIngredients(userInput$)).toBe('200ms a 401ms a 200ms b', { a: ['abc', 'cde'], b: ['cde'] });
      });

    });

    it('should suggest matching components on partial input', () => {
      fixture.detectChanges();
      getTestScheduler().run(helpers => {
        const { expectObservable } = helpers;
        const userInput$ = cold('a 300ms b 100ms a 200ms b', { a: 'c', b: 'cd' });
        expectObservable(component.searchComponents(userInput$)).toBe('200ms a 401ms a 200ms b', { a: ['abc', 'cde'], b: ['cde'] });
      });

    });

  });

  describe('submission', () => {

    describe('button', () => {

      it('should be disabled when ingForm is invalid', () => {
        component.editType = 'MODIFY';
        component.tsDto = {...tsDtoStub};
        fixture.detectChanges();
        component.ingForm.markAsDirty();
        component.analysisForm.markAsDirty();
        component.ingQuotasListFormArray.at(0).patchValue({ ingPerc: 101 });
        component.ingForm.updateValueAndValidity();
        component.analysisForm.updateValueAndValidity();
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('button#submit')).nativeElement.disabled).toBeTruthy();
      });

      it('should be disabled when analysisForm is invalid', () => {
        component.editType = 'MODIFY';
        component.tsDto = {...tsDtoStub};
        fixture.detectChanges();
        component.ingForm.markAsDirty();
        component.analysisForm.markAsDirty();
        component.analysisForm.get('rawProtein')?.patchValue('');
        component.ingForm.updateValueAndValidity();
        component.analysisForm.updateValueAndValidity();
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('button#submit')).nativeElement.disabled).toBeTruthy();
      });

      it('should be disabled when both forms are pristine', () => {
        component.editType = 'MODIFY';
        component.tsDto = {...tsDtoStub};
        fixture.detectChanges();
        component.ingForm.markAsPristine();
        component.analysisForm.markAsPristine();
        expect(fixture.debugElement.query(By.css('button#submit')).nativeElement.disabled).toBeTruthy();
      });

      it('should be enabled only when both forms are valid and at least one is dirty', () => {
        component.editType = 'MODIFY';
        component.tsDto = {...tsDtoStub};
        fixture.detectChanges();
        component.ingForm.markAsDirty();
        component.ingForm.updateValueAndValidity();
        component.analysisForm.updateValueAndValidity();
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('button#submit')).nativeElement.disabled).toBeFalsy();
        component.ingForm.markAsPristine();
        component.analysisForm.markAsDirty();
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('button#submit')).nativeElement.disabled).toBeFalsy();
      });

    });

    it('should dispatch action for new product', () => {
      component.editType = 'NEW';
      component.tsDto = {...tsDtoStub};
      fixture.detectChanges();
      component.onSubmit();
      expect(store.dispatch).toHaveBeenCalledWith(submitNewThirdStage({ id: component.id, formValue: component.tsDto }));
    });

    it('should dispatch action for editing existing product', () => {
      component.editType = 'MODIFY';
      component.tsDto = {...tsDtoStub};
      fixture.detectChanges();
      component.onSubmit();
      expect(store.dispatch).toHaveBeenCalledWith(submitEditThirdStage({ id: component.id, formValue: component.tsDto }));
    });

  });
});
