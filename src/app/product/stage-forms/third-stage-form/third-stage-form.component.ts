import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, AbstractControl, } from '@angular/forms';
import { moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';
import { takeUntil, debounceTime, map } from 'rxjs/operators';
import { Observable, ReplaySubject } from 'rxjs';
import { IngredientQuota } from 'src/app/model/ingredient-quota';
import { ComponentQuota } from 'src/app/model/component-quota';
import { TSDto } from 'src/app/model/tsdto';
import { faQuestionCircle, faTrashAlt, faCaretSquareDown } from '@fortawesome/free-regular-svg-icons';
import { faArrowsAlt, faPlus, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { IngredientService } from 'src/app/service/ingredient.service';
import { ComponentService } from 'src/app/service/component.service';
import { scaleTR } from 'src/app/animations';
import { Store } from '@ngrx/store';
import { submitEditThirdStage, submitNewThirdStage } from 'src/app/ngrx/product-edit/product-edit.actions';
import { initAnalysis } from 'src/app/model/analysis';

@Component({
  selector: 'app-third-stage-form',
  templateUrl: './third-stage-form.component.html',
  styleUrls: ['./third-stage-form.component.css'],
  animations: [scaleTR]
})
export class ThirdStageFormComponent implements OnInit, OnDestroy {

  @Input() editType = '';
  @Input() id = '';
  @Input() tsDto: TSDto = { ingQuotasList: [], analysis: initAnalysis() };

  cqCollapsed: boolean[] = [];

  ingForm: FormGroup;
  analysisForm: FormGroup;

  #suggestedIngredients: string[] = [];
  #suggestedComponents: string[] = [];

  iconHelp = faQuestionCircle;
  iconMove = faArrowsAlt;
  iconTrash = faTrashAlt;
  iconExpand = faCaretSquareDown;
  iconPlus = faPlus;
  iconPlusCircle = faPlusCircle;

  private destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(private appStore: Store,
    private formBuilder: FormBuilder,
    private ingService: IngredientService,
    private compService: ComponentService) {
    this.ingForm = this.buildIngForm();
    this.analysisForm = this.buildAnalysisForm();
  }

  ngOnInit() {

    if (this.editType === 'MODIFY') {
      this.populateForms();
    }
    else if (this.editType === 'NEW') {
      this.cqCollapsed.push(true);
    }

    this.ingService.get().pipe(
      takeUntil(this.destroy$))
      .subscribe(_ingredients => this.#suggestedIngredients = _ingredients);

    this.compService.get().pipe(
      takeUntil(this.destroy$))
      .subscribe(_components => this.#suggestedComponents = _components);
  }

  searchIngredients = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.#suggestedIngredients.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  searchComponents = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.#suggestedComponents.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  public get ingQuotasListFormArray() {
    return this.ingForm.get('ingQuotasList') as FormArray;
  }
  // temporary cast waiting to upgrade to typed forms
  public getCompQuotasListOf(iq: AbstractControl) {
    return (iq as FormGroup).get('compQuotasList') as FormArray;
  }

  addIq() {
    this.ingQuotasListFormArray.push(this.ingredientQuota);
    this.cqCollapsed.push(true);
  }

  rmIq(index: number) {
    this.ingQuotasListFormArray.removeAt(index);
    this.cqCollapsed.splice(index, 1);
  }
  // temporary cast waiting to upgrade to typed forms
  pushCqTo(iq: AbstractControl) {
    this.getCompQuotasListOf(iq as FormGroup).push(this.componentQuota);
  }
  // temporary cast waiting to upgrade to typed forms
  rmCq(iq: AbstractControl, index: number) {
    this.getCompQuotasListOf(iq as FormGroup).removeAt(index);
  }

  dropIq(event: CdkDragDrop<FormGroup>) {
    moveItemInArray(this.ingQuotasListFormArray.controls, event.previousIndex, event.currentIndex);
    moveItemInArray(this.ingQuotasListFormArray.value, event.previousIndex, event.currentIndex);
    this.ingForm.markAsDirty();
  }
  // temporary cast waiting to upgrade to typed forms
  dropCq(ing: AbstractControl, event: CdkDragDrop<FormGroup>) {
    moveItemInArray(this.getCompQuotasListOf(ing as FormGroup).controls, event.previousIndex, event.currentIndex);
    moveItemInArray(this.getCompQuotasListOf(ing as FormGroup).value, event.previousIndex, event.currentIndex);
    this.ingForm.markAsDirty();
  }

  get ingredientQuota(): FormGroup {

    return this.formBuilder.group({
      ingredient: ['', Validators.required],
      ingPerc: ['', [Validators.max(100), Validators.pattern('^[0-9]*$')]],
      compQuotasList: this.formBuilder.array([]),
    });
  }

  get componentQuota(): FormGroup {

    return this.formBuilder.group({
      component: ['', Validators.required],
      compPerc: ['', [Validators.max(100), Validators.pattern('^[0-9]*$')]]
    });
  }

  onSubmit() {
    this.tsDto.ingQuotasList = this.ingQuotasListFormArray.value;
    this.tsDto.analysis = this.analysisForm.value;

    if (this.editType === 'MODIFY') {
      this.appStore.dispatch(submitEditThirdStage({ id: this.id, formValue: this.tsDto }));
    }
    else if (this.editType === 'NEW') {
      this.appStore.dispatch(submitNewThirdStage({ id: this.id, formValue: this.tsDto }));
    }

  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  private buildIngForm() {

    return this.formBuilder.group({
      ingQuotasList: this.formBuilder.array([this.ingredientQuota])
    });
  }

  private buildAnalysisForm() {

    return this.formBuilder.group({

      rawProtein: [null, Validators.required],
      rawFibre: [null, Validators.required],
      rawOilFat: [null, Validators.required],
      rawAshes: [null, Validators.required],
      energy: [null, Validators.required],
      moisture: [null, Validators.required],

      calcium: [null],
      sodium: [null],
      phosphorus: [null]
    });
  }

  private populateForms() {

    this.ingQuotasListFormArray.clear();

    this.tsDto.ingQuotasList.forEach(
      (iq: IngredientQuota, iqIndex) => {
        this.ingQuotasListFormArray.push(this.ingredientQuota);
        iq.compQuotasList.forEach(
          (comp: ComponentQuota, compIndex) => {

            (this.getCompQuotasListOf(this.ingQuotasListFormArray.at(iqIndex) as FormGroup))
              .push(this.componentQuota);

            (this.getCompQuotasListOf(this.ingQuotasListFormArray.at(iqIndex) as FormGroup))
              .at(compIndex).patchValue(comp);
          }
        );
        this.ingQuotasListFormArray.at(iqIndex).patchValue(iq);

        this.cqCollapsed.push(true);
      }
    );

    for (const [key, val] of Object.entries(this.tsDto.analysis)) {
      this.analysisForm.patchValue({ [key]: val });
    }
  }


}
