import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HelpPopupModule } from 'src/app/share/modal/help-popup/help-popup.module';
import { ThirdStageFormComponent } from './third-stage-form.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgbCollapseModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    ThirdStageFormComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    HelpPopupModule,
    DragDropModule,
    NgbTypeaheadModule,
    NgbCollapseModule
  ],
  exports: [
    ThirdStageFormComponent
  ]
})
export class ThirdStageFormModule { }
