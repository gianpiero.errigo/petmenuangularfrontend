import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EMPTY } from 'rxjs';
import { productStub } from 'src/app/model/testing-mock';
import { getComparedProducts } from 'src/app/ngrx/compare/compare.selectors';
import { ProdService } from 'src/app/service/prod.service';
import { ProductViewComponent } from './product-view.component';
import { ProductViewModule } from './product-view.module';


describe('ProductViewComponent', () => {
  let component: ProductViewComponent;
  let fixture: ComponentFixture<ProductViewComponent>;
  let store: MockStore;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ProductViewModule],
      providers: [
        provideMockStore({
          initialState: {
            compare: { products: [] }
          }
        }),
        { provide: ProdService, useValue: {} },
        { provide: Router, useValue: {} },
        { provide: AuthService, useValue: {} },
        { provide: ActivatedRoute, useValue: { queryParams: EMPTY } }
      ]
    });

    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(ProductViewComponent);
    component = fixture.componentInstance;

  });

  it('should show only first stage component if _stage is 1', () => {
    component.product = { ...productStub, _stage: 1 };
    store.overrideSelector(getComparedProducts, []);

    store.refreshState();
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('app-first-stage-view'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('app-second-stage-view'))).toBeFalsy();
    expect(fixture.debugElement.query(By.css('app-third-stage-view'))).toBeFalsy();

  });

  it('should show only first 2 stage components if _stage is 2', () => {
    component.product = { ...productStub, _stage: 2 };
    store.overrideSelector(getComparedProducts, []);

    store.refreshState();
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('app-first-stage-view'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('app-second-stage-view'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('app-third-stage-view'))).toBeFalsy();

  });

  it('should show all 3 stage components if _stage is 3', () => {
    component.product = { ...productStub, _stage: 3 };
    store.overrideSelector(getComparedProducts, []);

    store.refreshState();
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('app-first-stage-view'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('app-second-stage-view'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('app-third-stage-view'))).toBeTruthy();

  });

  it('should highlight and show stamp if product is selected for comparison', waitForAsync(() => {
    component.product = { ...productStub, _stage: 3 };
    store.overrideSelector(getComparedProducts, [{...productStub}]);

    store.refreshState();
    fixture.detectChanges();

    fixture.whenRenderingDone().then(() => {
      const stamp = fixture.debugElement.query(By.css('div > .row > .col > div:nth-child(2) > div > div > span'));
      const highlightEl = fixture.debugElement.query(By.css('.transient-prod-view'));
      expect(stamp.nativeElement.textContent).toContain('Checked for comparison');
      expect(highlightEl.nativeElement.style.backgroundColor).toBe('rgba(var(--bs-tertiary-light-rgb), 0.7)');
    });
   
  }));

});
