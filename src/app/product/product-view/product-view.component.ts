import { Component, OnInit, Input } from '@angular/core';
import { map, Observable, ReplaySubject } from 'rxjs';
import { Product } from 'src/app/model/product';
import { fromTop, puffRight, selectedPress } from 'src/app/animations';
import { faCheckDouble } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { toggleCheckedForCompare } from 'src/app/ngrx/compare/compare.actions';
import { getComparedProducts } from 'src/app/ngrx/compare/compare.selectors';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
  animations: [fromTop, selectedPress, puffRight]
})

export class ProductViewComponent implements OnInit {

  //non-null assertion because this component can be composed only as ListProducts child
  //then product will always be defined and passed as input
  @Input() product!: Product;

  @Input() sortedByIng = '';
  @Input() sortedByAnalysis = '';
  @Input() ascendingOrder = false;

  isCheckedForCompare$: Observable<boolean> = new Observable<boolean>();

  iconChecked = faCheckDouble;

  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    private appState: Store,
  ) { }

  ngOnInit() {
    this.isCheckedForCompare$ = this.appState.select(getComparedProducts).pipe(
      map(comparedProducts =>comparedProducts.some(compared => compared.id ===  this.product.id))
    );
  }

  listenToCompare(posX: number, posY: number) {
    this.appState.dispatch(toggleCheckedForCompare({ product: this.product, posX, posY }));
  }

}
