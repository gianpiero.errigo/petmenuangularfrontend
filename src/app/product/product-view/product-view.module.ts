import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FirstStageViewModule } from '../stage-views/first-stage-view/first-stage-view.module';
import { SecondStageViewModule } from '../stage-views/second-stage-view/second-stage-view.module';
import { ThirdStageViewModule } from '../stage-views/third-stage-view/third-stage-view.module';
import { ProductViewComponent } from './product-view.component';


@NgModule({
  declarations: [
    ProductViewComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FirstStageViewModule,
    SecondStageViewModule,
    ThirdStageViewModule
  ],
  exports: [
    ProductViewComponent
  ]
})
export class ProductViewModule { }
