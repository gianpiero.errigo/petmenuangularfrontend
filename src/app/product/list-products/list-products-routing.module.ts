import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListProductsComponent } from './list-products.component';
import { ListProductsResolver } from './list-products.resolver';

const routes: Routes = [

  {
    path: '',
    component: ListProductsComponent,
    resolve: {_: ListProductsResolver},
    runGuardsAndResolvers: 'paramsOrQueryParamsChange'
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListProductsRoutingModule {}
