import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { DropdownModule } from 'src/app/share/modal/dropdown/dropdown.module';
import { ProductViewModule } from '../product-view/product-view.module';
import { ListProductsRoutingModule } from './list-products-routing.module';
import { ListProductsComponent } from './list-products.component';


@NgModule({
  declarations: [
    ListProductsComponent,
  ],
  imports: [
    CommonModule,
    ListProductsRoutingModule,
    FontAwesomeModule,
    ProductViewModule,
    DropdownModule,
    NgbPaginationModule,
  ],
  exports: [
    ListProductsComponent,
  ]
})
export class ListProductsModule { }
