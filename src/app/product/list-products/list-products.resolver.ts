import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, ParamMap, Resolve } from '@angular/router';
import { Store } from '@ngrx/store';
import { SearchParams } from 'src/app/model/app-state';
import { availableParamsKeys, isProdPackage, isProdType } from 'src/app/model/enums';
import { searchProducts } from 'src/app/ngrx/product-view/product-view.actions';

@Injectable({ providedIn: 'root' })
export class ListProductsResolver implements Resolve<void> {

  availableParamsKeys = [ ...availableParamsKeys ];

  constructor(private readonly appState: Store) { }

  resolve(route: ActivatedRouteSnapshot): void {
    this.appState.dispatch(searchProducts({ searchParams: this.buildSearchParams(route.queryParamMap) }));
  }

  //TODO: to be refactored after upgrade to typed forms
  buildSearchParams(query: ParamMap): SearchParams {
    return this.availableParamsKeys.reduce<SearchParams>((obj, key) => {
      const value = query.get(key);
      if (value !== null) {
        if(key === 'prodType') {
          if(isProdType(value)) {
            obj.prodType = value;
          }
        }
        else if(key === 'prodPackage') {
          if(isProdPackage(value)) {
            obj.prodPackage = value;
          }
        }
        else {
          obj[key] = value.trim();
        }
      }
      return obj;
    }, {} as SearchParams);
  }
}
