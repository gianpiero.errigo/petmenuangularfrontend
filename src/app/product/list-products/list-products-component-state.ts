import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { Store } from '@ngrx/store';
import { EMPTY, Observable } from 'rxjs';
import { catchError, tap, withLatestFrom } from 'rxjs/operators';
import { Analysis } from 'src/app/model/analysis';
import { Product } from 'src/app/model/product';
import { getSearchResults } from 'src/app/ngrx/product-view/product-view.selectors';
import { SortingService } from 'src/app/service/sorting.service';

export interface ProductsListState {
  sortedByIng: string;
  sortedByAnalysis: string;
  ascending: boolean;
  sortedList: Array<Product>;
}

@Injectable()
export class ProductsListStore extends ComponentStore<ProductsListState> {

  //SELECTORS
  readonly sortedByIng$: Observable<string> = this.select(state => state.sortedByIng);
  readonly sortedByAnalysis$: Observable<string> = this.select(state => state.sortedByAnalysis);
  readonly ascending$: Observable<boolean> = this.select(state => state.ascending);
  readonly sortedList$: Observable<Array<Product>> = this.select(state => state.sortedList);

  //WRITERS
  revertSorting = this.updater(state => ({ ...state, sortedList: state.sortedList.reverse(), ascending: !state.ascending }));
  resetSortingState = this.setState({ sortedByIng: '', sortedByAnalysis: '', ascending: false, sortedList: [] });

  //EFFECTS
  readonly sortByIng = this.effect((ing$: Observable<string>) => ing$.pipe(
    withLatestFrom(this.appState.select(getSearchResults)),
    tap(([ing, prodList]) => ing === this.get(state => state.sortedByIng)
      ? this.revertSorting()
      : this.setState({
        sortedByIng: ing,
        sortedByAnalysis: '',
        sortedList: this.sortService.sortByIng(ing, prodList),
        ascending: false
      })
    ),
    catchError(() => EMPTY)
  ));

  readonly sortByAnalysis = this.effect((analysis$: Observable<keyof Analysis>) => analysis$.pipe(
    withLatestFrom(this.appState.select(getSearchResults)),
    tap(([element, prodList]) => element === this.get(state => state.sortedByAnalysis)
      ? this.revertSorting()
      : this.setState({
        sortedByIng: '',
        sortedByAnalysis: element,
        sortedList: this.sortService.sortByAnalysis(element, prodList),
        ascending: false
      })
    ),
    catchError(() => EMPTY)
  ));

  constructor(private appState: Store,
    private sortService: SortingService) {
    super({ sortedByIng: '', sortedByAnalysis: '', ascending: false, sortedList: [] });
  }

}
