import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, merge, ReplaySubject } from 'rxjs';
import { takeUntil, filter, map, tap } from 'rxjs/operators';
import { ActivatedRoute, Router, NavigationStart } from '@angular/router';
import { fromTop, dropDown, paginator } from 'src/app/animations';
import { locProdTypes, locProdPackages, locAvailableParamsKeys, availableParamsKeys } from 'src/app/model/enums';
import { AnimationEvent } from '@angular/animations';
import { Store } from '@ngrx/store';
import { getSearchParams, getSearchResults } from 'src/app/ngrx/product-view/product-view.selectors';
import { SearchParams } from 'src/app/model/app-state';
import { GramToKiloPipe } from '../pipe/gram-to-kilo.pipe';
import { ProductsListStore } from './list-products-component-state';


@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss'],
  providers: [ProductsListStore],
  animations: [fromTop, dropDown, paginator]
})

export class ListProductsComponent implements OnInit, OnDestroy {

  availableParamsKeys = [...availableParamsKeys];
  locAvailableParamsKeys = { ...locAvailableParamsKeys };

  _localizedOrder = {
    ascending: $localize`:order@@asc:Ascending`,
    descending: $localize`:order@@desc:Descending`
  };

  locProdTypes = { ...locProdTypes };
  locProdPackages = { ...locProdPackages };

  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  localizedParams$ = this.appState.select(getSearchParams).pipe(
    takeUntil(this.destroy$),
    filter(params => !!params),
    map(params => this.localizeParams(params))
  );

  vm$ = combineLatest([
    merge(this.productsListStore.sortedList$, this.appState.select(getSearchResults)),
    this.productsListStore.sortedByIng$,
    this.productsListStore.sortedByAnalysis$,
    this.productsListStore.ascending$,
    this.localizedParams$
  ]).pipe(
    tap(() => {
      this.page = 1;
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }),
    map(([productsList, sortedByIng, sortedByAnalysis, ascending, localizedParams]) =>
      ({ productsList, sortedByIng, sortedByAnalysis, ascending, localizedParams })
    )
  );

  page = 1;
  pageSize = 5;
  changePageState = '';
  newPage = 1;

  constructor(public route: ActivatedRoute,
    private router: Router,
    private appState: Store,
    private productsListStore: ProductsListStore,
    private gramToKilo: GramToKiloPipe) { }

  ngOnInit(): void {

    this.router.events.pipe(
      filter(event => event instanceof NavigationStart))
      .subscribe(() => this.productsListStore.resetSortingState);

  }

  localizeParams(params: SearchParams) {
    const localizedParams: Array<{ key: string; value: string }> = [];
    for (const paramKey of this.availableParamsKeys) {
      if (params[paramKey]) {
        if (paramKey === 'prodType') {
          const paramValue = params[paramKey];
          if (paramValue) {
            localizedParams.push({ key: locAvailableParamsKeys.prodType, value: locProdTypes[paramValue] });
          }
        }
        else if (paramKey === 'prodPackage') {
          const paramValue = params[paramKey];
          if (paramValue) {
            localizedParams.push({ key: locAvailableParamsKeys.prodPackage, value: locProdPackages[paramValue] });
          }
        }
        else if (paramKey === 'minWeight') {
          const paramValue = params[paramKey];
          if (paramValue !== '0') {
            localizedParams.push({ key: locAvailableParamsKeys.minWeight, value: this.gramToKilo.transform(paramValue) });
          }
        }
        else if (paramKey === 'maxWeight') {
          const paramValue = params[paramKey];
          if (paramValue !== '0') {
            localizedParams.push({ key: locAvailableParamsKeys.maxWeight, value: this.gramToKilo.transform(paramValue) });
          }
        }
        else {
          const paramValue = params[paramKey];
          if (paramValue) {
            localizedParams.push({ key: locAvailableParamsKeys[paramKey], value: paramValue });
          }
        }
      }
    }

    return localizedParams;
  }

  leavingForPage(newPage: number) {
    newPage > this.page ? this.changePageState = 'leaveForNextPage' : this.changePageState = 'leaveForPrevPage';
    this.newPage = newPage;
  }

  enteringPage(ev: AnimationEvent) {
    this.page = this.newPage;
    if(ev.toState === 'leaveForNextPage') {
      this.changePageState = 'enterNextPage';
    }
    else if(ev.toState === 'leaveForPrevPage') {
      this.changePageState = 'enterPrevPage';
    }
    else {
      this.changePageState = ''
    }
    window.scroll({ top: 0, left: 0, behavior: 'smooth' });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
