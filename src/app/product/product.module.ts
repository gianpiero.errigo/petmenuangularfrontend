import { NgModule } from '@angular/core';
import { ProductRoutingModule } from './product-routing.module';
import { ProductViewStateModule } from '../ngrx/product-view/product-view-state.module';
import { CompareStateModule } from '../ngrx/compare/compare-state.module';
import { GramToKiloPipe } from './pipe/gram-to-kilo.pipe';

@NgModule({
  imports: [
    ProductRoutingModule,
    ProductViewStateModule,
    CompareStateModule,
  ],
  providers: [
    GramToKiloPipe,
  ]
})
export class ProductModule { }
