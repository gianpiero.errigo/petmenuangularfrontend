import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { fsDtoStub } from 'src/app/model/testing-mock';
import { ProdService } from 'src/app/service/prod.service';
import { DropdownDirective } from 'src/app/share/modal/dropdown/dropdown.directive';
import { GramToKiloModule } from '../../pipe/gram-to-kilo.module';

import { FirstStageViewComponent } from './first-stage-view.component';
import { FirstStageViewModule } from './first-stage-view.module';

describe('FirstStageViewComponent', () => {
  let component: FirstStageViewComponent;
  let fixture: ComponentFixture<FirstStageViewComponent>;
  let routerSpy: jasmine.SpyObj<Router>;

  beforeEach(() => {
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    TestBed.configureTestingModule({
      providers: [
        { provide: ProdService, useValue: {} },
        { provide: Router, useValue: routerSpy },
        { provide: AuthService, useValue: {} },
      ],
      imports: [FirstStageViewModule, GramToKiloModule]
    });

    fixture = TestBed.createComponent(FirstStageViewComponent);
    component = fixture.componentInstance;
    component.fsDto = fsDtoStub;
    fixture.detectChanges();
  });

  describe('button for editing', () => {

    it('should NOT show if in editing mode', () => {
      component.editing = true;

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(DropdownDirective))).toBeFalsy();
    });

    it('should NOT show if in listing mode', () => {
      component.listing = true;

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(DropdownDirective))).toBeFalsy();
    });

    it('should show in any other case', () => {
      component.listing = false;
      component.editing = false;

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(DropdownDirective))).toBeTruthy();
    });

  });

  describe('buttons near three main data labels', () => {

    it('first one should route to the right Brand search', (fakeAsync(() => {
      component.listing = true;
      fixture.detectChanges();
      const goToButtons = fixture.debugElement.queryAll(By.css('button'));
      goToButtons[0].nativeElement.click();
      fixture.whenStable().then(() => {
        expect(routerSpy.navigate)
          .toHaveBeenCalledOnceWith(['product/search'], { queryParams: { brand: 'brand' } });
      });
    })));

    it('second one should route to the right Line search', (fakeAsync(() => {
      component.listing = true;
      fixture.detectChanges();
      const goToButtons = fixture.debugElement.queryAll(By.css('button'));
      goToButtons[1].nativeElement.click();
      fixture.whenStable().then(() => {
        expect(routerSpy.navigate)
          .toHaveBeenCalledOnceWith(['product/search'], { queryParams: { brand: 'brand', line: 'line' } });
      });
    })));

    it('third one should route to the right Recipe search', (fakeAsync(() => {
      component.listing = true;
      fixture.detectChanges();
      const goToButtons = fixture.debugElement.queryAll(By.css('button'));
      goToButtons[2].nativeElement.click();
      fixture.whenStable().then(() => {
        expect(routerSpy.navigate)
          .toHaveBeenCalledOnceWith(['product/search'], { queryParams: { brand: 'brand', line: 'line', recipe: 'recipe' } });
      });
    })));

  });


});
