import { Component, Input, OnDestroy } from '@angular/core';
import { FSDto, initFSDto } from 'src/app/model/fsdto';
import { locProdPackages, locProdTypes } from 'src/app/model/enums';
import { ReplaySubject } from 'rxjs';
import { faEdit, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';
import { takeUntil, filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ProdService } from 'src/app/service/prod.service';
import { dropDown } from 'src/app/animations';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-first-stage-view',
  templateUrl: './first-stage-view.component.html',
  styleUrls: ['./first-stage-view.component.css'],
  animations: [dropDown]
})
export class FirstStageViewComponent implements OnDestroy {

  @Input() fsDto: FSDto = initFSDto();
  @Input() id = '';
  @Input() curStage = 0;
  @Input() listing = false;
  @Input() editing = false;

  locProdTypes = { ...locProdTypes };
  locProdPackages = { ...locProdPackages };

  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  iconEdit = faEdit;
  iconGoto = faAngleDoubleRight;
  iconTemp = faQuestionCircle;

  constructor(private prodService: ProdService,
    public router: Router,
    public auth: AuthService) { }

  onEditStage(stageToInput: number) {
    const editType = (stageToInput > this.curStage) ? 'NEW' : 'MODIFY';
    this.router.navigate(['product/', 'edit', this.id, stageToInput, editType]);
  }

  deleteProduct() {
    this.prodService.deleteProduct(this.id).pipe(
      takeUntil(this.destroy$),
      filter(info => info === 'ok'))
      .subscribe(() => this.router.navigate(['/home']));
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }


}
