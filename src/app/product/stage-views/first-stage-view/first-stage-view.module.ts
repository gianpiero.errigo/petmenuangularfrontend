import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FirstStageViewComponent } from './first-stage-view.component';
import { DropdownModule } from 'src/app/share/modal/dropdown/dropdown.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { GramToKiloModule } from '../../pipe/gram-to-kilo.module';
import { HasRoleModule } from 'src/app/share/directives/has-role.module';
import { TouchTooltipModule } from 'src/app/share/modal/touch-tooltip/touch-tooltip.module';


@NgModule({
  declarations: [
    FirstStageViewComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    DropdownModule,
    NgbTooltipModule,
    GramToKiloModule,
    HasRoleModule,
    TouchTooltipModule
  ],
  exports: [
    FirstStageViewComponent
  ]
})
export class FirstStageViewModule { }
