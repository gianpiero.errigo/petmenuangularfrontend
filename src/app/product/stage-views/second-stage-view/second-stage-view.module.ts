import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SecondStageViewComponent } from './second-stage-view.component';
import { ZoomImageModule } from '../../zoom-image/zoom-image.module';


@NgModule({
  declarations: [
    SecondStageViewComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ZoomImageModule
  ],
  exports: [
    SecondStageViewComponent
  ]
})
export class SecondStageViewModule { }
