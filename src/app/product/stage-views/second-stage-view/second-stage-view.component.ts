// eslint-disable-next-line max-len
import { Component, Input, ViewContainerRef, ViewChild, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { initSSDto, SSDto } from 'src/app/model/ssdto';
import { faAngleDoubleRight, faSearchPlus } from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ZoomImageComponent } from '../../zoom-image/zoom-image.component';

@Component({
  selector: 'app-second-stage-view',
  templateUrl: './second-stage-view.component.html',
  styleUrls: ['./second-stage-view.component.scss'],
})
export class SecondStageViewComponent implements OnInit, OnDestroy {

  @Input() ssDto: SSDto = initSSDto();
  @Input() checkedForCompare = false;
  @Input() listing = false;
  @Input() editing? = false;

  @Output() tellCheckedForCompare: EventEmitter<{ posX: number; posY: number }> = new EventEmitter<{ posX: number; posY: number }>();

  @ViewChild('zoomContainer', { read: ViewContainerRef, static: false }) container!: ViewContainerRef;

  #id = '';
  @Input() set id(newId) {
    this.#id = newId;
    this.imgsSrc = {
      frontImg: `${environment.pmImgs}/${newId}/frontImg_sm.jpg`,
      backImg: `${environment.pmImgs}/${newId}/backImg_sm.jpg`,
      detailsOneImg: `${environment.pmImgs}/${newId}/detailsOneImg_sm.jpg`,
      detailsTwoImg: `${environment.pmImgs}/${newId}/detailsTwoImg_sm.jpg`,
    };
  }
  get id(): string {
    return this.#id;
  }

  imgsSrc = { frontImg: '', backImg: '', detailsOneImg: '', detailsTwoImg: '' };

  iconZoom = faSearchPlus;
  iconGoto = faAngleDoubleRight;

  destroy$: Subject<boolean> = new Subject();

  constructor(public router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    // Cachebuster hack when redirecting from changing images
    this.route.queryParams.pipe(
      takeUntil(this.destroy$),
      filter(({ updateImgs }) => updateImgs)
    ).subscribe(() =>
      this.imgsSrc = Object.assign({}, ...Object.entries(this.imgsSrc).map(([slot, src]) =>
        ({ [slot]: `${src}?${Date.now()}` }))));
    ///////////////////////////////////////////////////////////

  }

  showZoom($event: MouseEvent, id: string, slot: string) {
    $event.stopPropagation();
    const componentRef = this.container.createComponent(ZoomImageComponent);
    this.container.get(1)?.destroy();

    componentRef.instance.id = id;
    componentRef.instance.slot = slot;
    componentRef.instance.closeZoom.subscribe(() => this.closeZoom());

    componentRef.location.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });

  }

  closeZoom() {
    this.container.clear();
  }

  toggleCheckForCompare($event: MouseEvent) {
    this.tellCheckedForCompare.emit({ posX: $event.clientX, posY: $event.clientY });
  }

  ngOnDestroy() {
    this.container.clear();
    this.destroy$.next(true);
    this.destroy$.complete();
  }


}
