import { ComponentFactoryResolver, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { ssDtoStub } from 'src/app/model/testing-mock';
import { environment } from 'src/environments/environment';

import { SecondStageViewComponent } from './second-stage-view.component';
import { SecondStageViewModule } from './second-stage-view.module';

describe('SecondStageViewComponent', () => {
  let component: SecondStageViewComponent;
  let fixture: ComponentFixture<SecondStageViewComponent>;
  let imgsEl: DebugElement[];


  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [SecondStageViewModule],
      providers: [
        { provide: ComponentFactoryResolver, useValue: {} },
        { provide: Router, useValue: {} },
        { provide: ActivatedRoute, useValue: { queryParams: EMPTY } }
      ]
    });

    fixture = TestBed.createComponent(SecondStageViewComponent);
    component = fixture.componentInstance;
    component.id = '99';
    component.ssDto = ssDtoStub;
    fixture.detectChanges();
    imgsEl = fixture.debugElement.queryAll(By.css('img'));

  });

  describe('show images registered for product', () => {

    it('should hide zoom button in listing mode', () => {
      component.listing = true;
      fixture.detectChanges();
      const imgContainers = fixture.debugElement.queryAll(By.css('div > div > div > div'));
      expect(imgContainers[0].query(By.css('button'))).toBeNull();
    });

    it('should show front image', () => {
      expect(imgsEl[0].nativeElement.getAttribute('src') === `${environment.pmImgs}/99/frontImg_sm.jpg`)
        .toBeTruthy();
    });

    it('should show back image', () => {
      expect(imgsEl[1].nativeElement.getAttribute('src') === `${environment.pmImgs}/99/backImg_sm.jpg`)
        .toBeTruthy();
    });

    it('should show first detail image', () => {
      expect(imgsEl[2].nativeElement.getAttribute('src') === `${environment.pmImgs}/99/detailsOneImg_sm.jpg`)
        .toBeTruthy();
    });

    it('should show second detail image', () => {
      expect(imgsEl[3].nativeElement.getAttribute('src') === `${environment.pmImgs}/99/detailsTwoImg_sm.jpg`)
        .toBeTruthy();
    });

  });

  it('should NOT show View button and shops in editing mode', () => {
    component.editing = true;
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('div')).children.length === 1).toBeTruthy();
  });

  it('should show View button in listing mode', () => {
    component.listing = true;
    fixture.detectChanges();
    const shopsDiv = fixture.debugElement.query(By.css('div')).children;
    expect(shopsDiv[1].queryAll(By.css('button')).find(b => b.nativeElement.textContent === 'View')).toBeTruthy();
  });

});
