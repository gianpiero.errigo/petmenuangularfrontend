import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { tsDtoStub } from 'src/app/model/testing-mock';

import { ThirdStageViewComponent } from './third-stage-view.component';
import { ThirdStageViewModule } from './third-stage-view.module';

describe('ThirdStageViewComponent', () => {
  let component: ThirdStageViewComponent;
  let fixture: ComponentFixture<ThirdStageViewComponent>;
  let ingContainer: DebugElement;
  let analysisContainer: HTMLElement;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ThirdStageViewModule]
    });

    fixture = TestBed.createComponent(ThirdStageViewComponent);
    component = fixture.componentInstance;

    component.tsDto = tsDtoStub;
    component.vm$ = of({ sortedByIng: '', sortedByAnalysis: '', ascending: false });
    fixture.detectChanges();

    ingContainer = fixture.debugElement.query(By.css('.row > .col > .row:nth-child(2) > .col'));
    analysisContainer = fixture.nativeElement.querySelector((':scope > .row:nth-child(2) > .col > .row:nth-child(2) > .col'));
  });

  it('should show ingredients in correct order', () => {
    expect(ingContainer.children.length === 3).toBeTruthy();
    expect(ingContainer.children[0]
      .query(By.css('.col > .row > .col-8 > div > span')).nativeElement.textContent).toBe('ing1');
    expect(ingContainer.children[0]
      .query(By.css('.col > .row > .col:last-of-type > div > div > span')).nativeElement.textContent).toBe('80%');
    expect(ingContainer.children[1]
      .query(By.css('.col > .row > .col-8 > div > span')).nativeElement.textContent).toBe('ing2');
    expect(ingContainer.children[1]
      .query(By.css('.col > .row > .col:last-of-type > div > div > span')).nativeElement.textContent).toBe('10%');
    expect(ingContainer.children[2]
      .query(By.css('.col > .row > .col-8 > div > span')).nativeElement.textContent).toBe('ing3');
    expect(ingContainer.children[2]
      .query(By.css('.col > .row > .col:last-of-type > div > div > span')).nativeElement.textContent).toBe('-');
  });

  describe('data presented for every ingredient', () => {

    it('should show components in correct order for ing1', () => {
      const compContainer = ingContainer.children[0].query(By.css('.col > .row > .col-8 > .flex-column'));
      expect(compContainer.children[0].query(By.css('div:first-child > div > span')).nativeElement.textContent).toBe('comp1_A');
      expect(compContainer.children[0].query(By.css('div:last-child > div > span')).nativeElement.textContent).toBe('70%');
      expect(compContainer.children[1].query(By.css('div:first-child > div > span')).nativeElement.textContent).toBe('comp1_B');
      expect(compContainer.children[1].query(By.css('div:last-child > div > span')).nativeElement.textContent).toBe('-');
    });

    it('should show NO components for ing2', () => {
      expect(ingContainer.children[1].query(By.css('.col > .row > .col-8 > .flex-column')).children.length).toBe(0);
    });

    it('should show components in correct order for ing3', () => {
      const compContainer = ingContainer.children[2].query(By.css('.col > .row > .col-8 > .flex-column'));
      expect(compContainer.children[0].query(By.css('div:first-child > div > span')).nativeElement.textContent).toBe('comp3_A');
      expect(compContainer.children[0].query(By.css('div:last-child > div > span')).nativeElement.textContent).toBe('80%');
      expect(compContainer.children[1].query(By.css('div:first-child > div > span')).nativeElement.textContent).toBe('comp3_B');
      expect(compContainer.children[1].query(By.css('div:last-child > div > span')).nativeElement.textContent).toBe('-');
      expect(compContainer.children[2].query(By.css('div:first-child > div > span')).nativeElement.textContent).toBe('comp3_C');
      expect(compContainer.children[2].query(By.css('div:last-child > div > span')).nativeElement.textContent).toBe('30%');

    });
  });

  describe('data presented for analysis', () => {
    it('should show analysis data', () => {
      expect(analysisContainer.children[0].querySelector('.row > div:first-child span')?.textContent).toBe('Protein');
      expect(analysisContainer.children[0].querySelector('.row > div:nth-child(2) span')?.textContent).toBe('9%');
      expect(analysisContainer.children[1].querySelector('.row > div:first-child span')?.textContent).toBe('Fibre');
      expect(analysisContainer.children[1].querySelector('.row > div:nth-child(2) span')?.textContent).toBe('8%');
      expect(analysisContainer.children[2].querySelector('.row > div:first-child span')?.textContent).toBe('Oil and Fat');
      expect(analysisContainer.children[2].querySelector('.row > div:nth-child(2) span')?.textContent).toBe('7%');
      expect(analysisContainer.children[3].querySelector('.row > div:first-child span')?.textContent).toBe('Ashes');
      expect(analysisContainer.children[3].querySelector('.row > div:nth-child(2) span')?.textContent).toBe('6%');
      expect(analysisContainer.children[4].querySelector('.row > div:first-child span')?.textContent).toBe('Calcium');
      expect(analysisContainer.children[4].querySelector('.row > div:nth-child(2) span')?.textContent).toBe('5%');
      expect(analysisContainer.children[5].querySelector('.row > div:first-child span')?.textContent).toBe('Sodium');
      expect(analysisContainer.children[5].querySelector('.row > div:nth-child(2) span')?.textContent).toBe('4%');
      expect(analysisContainer.children[6].querySelector('.row > div:first-child span')?.textContent).toBe('Moisture');
      expect(analysisContainer.children[6].querySelector('.row > div:nth-child(2) span')?.textContent).toBe('60%');
      expect(analysisContainer.children[7].querySelector('.row > div:first-child span')?.textContent).toBe('Energy');
      expect(analysisContainer.children[7].querySelector('.row > div:nth-child(2) span')?.textContent).toBe('99 Kcal');
    });

    it('should NOT show any entry for Phosphorus', () => {
      expect(Array.from(analysisContainer.children)
        .find(c => c.querySelector('.row > div:first-child span')?.textContent === 'Phosphorus'))
        .toBeFalsy();
    });
  });

});
