import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { ThirdStageViewComponent } from './third-stage-view.component';
import { TouchTooltipModule } from 'src/app/share/modal/touch-tooltip/touch-tooltip.module';


@NgModule({
  declarations: [
    ThirdStageViewComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    NgbCollapseModule,
    TouchTooltipModule,
  ],
  exports: [
    ThirdStageViewComponent,
  ]
})
export class ThirdStageViewModule { }
