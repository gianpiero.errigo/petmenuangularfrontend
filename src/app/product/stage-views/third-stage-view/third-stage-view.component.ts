import { Component, Input, Optional } from '@angular/core';
import { TSDto } from 'src/app/model/tsdto';
import { faChevronCircleDown, faSortAmountDown, faSortAmountDownAlt } from '@fortawesome/free-solid-svg-icons';
import { combineLatest, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IngredientQuota } from 'src/app/model/ingredient-quota';
import { ProductsListStore } from '../../list-products/list-products-component-state';
import { analysisKeys, initLocAnalysis } from 'src/app/model/enums';


@Component({
  selector: 'app-third-stage-view',
  templateUrl: './third-stage-view.component.html',
  styleUrls: ['./third-stage-view.component.css']
})
export class ThirdStageViewComponent {

  #listing = false;
  @Input() set listing(isListing: boolean) {
    this.#listing = isListing;
    this.analysisCollapsed = isListing;
    this.ingCollapsed = isListing;
  }
  get listing() { return this.#listing; }

  @Input() set tsDto(newTsDto: TSDto) {
    this.ingQuotasList = newTsDto.ingQuotasList;
    analysisKeys.forEach(elem => this.analysis[elem].value = newTsDto.analysis[elem] ?? 0);
  }

  ingQuotasList: IngredientQuota[] = [];
  analysis = initLocAnalysis();
  analysisKeys = [...analysisKeys];

  compCollapsed: boolean[] = [];
  analysisCollapsed = false;
  ingCollapsed = false;

  iconDropDown = faChevronCircleDown;
  iconSortDesc = faSortAmountDown;
  iconSortAsc = faSortAmountDownAlt;

  vm$: Observable<{ sortedByIng: string; sortedByAnalysis: string; ascending: boolean }>;

  constructor(@Optional() public productsListStore: ProductsListStore) {
    this.compCollapsed = new Array<boolean>(this.ingQuotasList.length).fill(true);

    if (productsListStore) {
      this.vm$ = combineLatest([
        this.productsListStore.sortedByIng$,
        this.productsListStore.sortedByAnalysis$,
        this.productsListStore.ascending$
      ])
        .pipe(
          map(([sortedByIng, sortedByAnalysis, ascending]) => ({ sortedByIng, sortedByAnalysis, ascending }))
        );
    }
    // Workaround to satisfy top *ngIf used to async vm$ in product-detail-view
    else { this.vm$ = of({ sortedByIng: '', sortedByAnalysis: '', ascending: false }); }

  }


}
