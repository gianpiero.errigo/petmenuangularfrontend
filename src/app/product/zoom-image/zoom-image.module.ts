import { NgModule } from '@angular/core';
import { RedZoomModule } from 'ngx-red-zoom';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ZoomImageComponent } from './zoom-image.component';

@NgModule({
  declarations: [
    ZoomImageComponent
  ],
  imports: [
    FontAwesomeModule,
    RedZoomModule,
  ],
  exports: [
    ZoomImageComponent
  ]
})
export class ZoomImageModule { }
