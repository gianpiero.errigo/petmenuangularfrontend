import { Component, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { faTimesCircle, faHandPointLeft } from '@fortawesome/free-regular-svg-icons';
import { environment } from 'src/environments/environment';
import { scale, triggerChildLeave } from 'src/app/animations';

@Component({
  selector: 'app-zoom-image',
  templateUrl: './zoom-image.component.html',
  styleUrls: ['./zoom-image.component.scss'],
  animations: [triggerChildLeave, scale]
})
export class ZoomImageComponent {

  @HostBinding('@triggerChildLeave') get triggerChildLeave() { return true; }

  @Input() id = '';
  @Input() slot = '';

  @Output() closeZoom: EventEmitter<string> = new EventEmitter<string>();

  faPointLeft = faHandPointLeft;
  faClose = faTimesCircle;
  imgsDir = environment.pmImgs;

  onClose() {
    this.closeZoom.emit('close');
  }

}
