import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomImageComponent } from './zoom-image.component';
import { ZoomImageModule } from './zoom-image.module';

describe('ZoomImageComponent', () => {
  let component: ZoomImageComponent;
  let fixture: ComponentFixture<ZoomImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ZoomImageModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
