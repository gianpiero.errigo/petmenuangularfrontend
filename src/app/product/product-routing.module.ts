import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  { path: 'view', loadChildren: () => import(`./product-detail-view/product-detail-view.module`).then(m => m.ProductDetailViewModule) },

  { path: 'edit', loadChildren: () => import(`./product-edit/product-edit.module`).then(m => m.ProductEditModule) },

  { path: 'search', loadChildren: () => import(`./list-products/list-products.module`).then(m => m.ListProductsModule) },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductRoutingModule { }
