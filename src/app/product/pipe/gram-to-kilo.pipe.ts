import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'gramToKilo'})
export class GramToKiloPipe implements PipeTransform {
  transform(value: string|number): string {
    return +value > 999 ? `${parseFloat((+value / 1000).toFixed(2))} Kg` : `${value} gr`;
  }
}
