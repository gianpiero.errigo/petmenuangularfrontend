import { NgModule } from '@angular/core';
import { GramToKiloPipe } from './gram-to-kilo.pipe';

@NgModule({
  declarations: [
    GramToKiloPipe,
  ],
  exports: [
    GramToKiloPipe
  ]
})
export class GramToKiloModule { }
