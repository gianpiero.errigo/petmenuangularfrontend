import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Store } from '@ngrx/store';
import { editProduct } from 'src/app/ngrx/product-edit/product-edit.actions';

@Injectable({ providedIn: 'root' })
export class ProductEditResolver implements Resolve<void> {

  constructor(private readonly appState: Store) { }

  resolve(route: ActivatedRouteSnapshot): void {

    const checkEditTypeParam = (editTypeRouteParam: string): editTypeRouteParam is 'NEW' | 'MODIFY' => {
      if (!(['NEW', 'MODIFY'].includes(editTypeRouteParam))) {
        throw new Error(`Invalid editType route param: "${editTypeRouteParam}" is neither "NEW" nor "MODIFY"`);
      }
      else { return true; }
    };

    const editTypeParam = route.paramMap.get('editType');
    const editIdParam = route.paramMap.get('id');
    const editStageParam = route.paramMap.get('editStage');
    if (editTypeParam && editIdParam && editStageParam) {
      if (checkEditTypeParam(editTypeParam)) {
        this.appState.dispatch(
          editProduct({
            prodEditId: editIdParam,
            prodEditType: editTypeParam,
            prodEditStage: parseInt(editStageParam, 10)
          })
        );
      }
    }
  }
}
