import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@auth0/auth0-angular';
import { FirstStageFormComponent } from '../stage-forms/first-stage-form/first-stage-form.component';
import { ProductEditComponent } from './product-edit.component';
import { ProductEditResolver } from './product-edit.resolver';

const routes: Routes = [

  {
    path: 'new',
    component: FirstStageFormComponent,
    canActivate: [AuthGuard]
  },

  {
    path: ':id/:editStage/:editType',
    component: ProductEditComponent,
    resolve: {_: ProductEditResolver},
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
    canActivate: [AuthGuard]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductEditRoutingModule {}
