import { Component } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { ProdService } from 'src/app/service/prod.service';
import { Store } from '@ngrx/store';
import { getProductEditState } from 'src/app/ngrx/product-edit';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})

export class ProductEditComponent implements OnDestroy {

  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  vm$ = this.appState.select(getProductEditState).pipe(
    filter(editState => !!editState.prodEditId),
    switchMap(editState => this.prodService.getProductById(editState.prodEditId).pipe(
      map(prod => ({ prod, editState }))
    )),
    takeUntil(this.destroy$)
  );

  constructor(
    private appState: Store,
    private prodService: ProdService) { }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
