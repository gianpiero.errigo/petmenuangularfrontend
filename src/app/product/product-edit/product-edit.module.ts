import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ProductEditStateModule } from 'src/app/ngrx/product-edit/product-edit-state.module';
import { FirstStageFormModule } from '../stage-forms/first-stage-form/first-stage-form.module';
import { SecondStageFormModule } from '../stage-forms/second-stage-form/second-stage-form.module';
import { ThirdStageFormModule } from '../stage-forms/third-stage-form/third-stage-form.module';
import { FirstStageViewModule } from '../stage-views/first-stage-view/first-stage-view.module';
import { SecondStageViewModule } from '../stage-views/second-stage-view/second-stage-view.module';
import { ThirdStageViewModule } from '../stage-views/third-stage-view/third-stage-view.module';
import { ProductEditRoutingModule } from './product-edit-routing.module';
import { ProductEditComponent } from './product-edit.component';


@NgModule({
  declarations:[
    ProductEditComponent,
  ],
  imports: [
    CommonModule,
    ProductEditRoutingModule,
    ProductEditStateModule,
    FirstStageViewModule,
    SecondStageViewModule,
    ThirdStageViewModule,
    FirstStageFormModule,
    SecondStageFormModule,
    ThirdStageFormModule
  ],
  exports: [
    ProductEditComponent
  ]
})
export class ProductEditModule { }
