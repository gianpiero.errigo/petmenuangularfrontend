import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EMPTY, ReplaySubject } from 'rxjs';
import { Product } from 'src/app/model/product';
import { productStub } from 'src/app/model/testing-mock';
import { getProductEditState } from 'src/app/ngrx/product-edit';
import { BrandService } from 'src/app/service/brand.service';
import { ComponentService } from 'src/app/service/component.service';
import { IngredientService } from 'src/app/service/ingredient.service';
import { LineService } from 'src/app/service/line.service';
import { ProdService } from 'src/app/service/prod.service';
import { ProductEditComponent } from './product-edit.component';
import { RootStateModule } from 'src/app/ngrx/root-state-module';
import { ProductEditModule } from './product-edit.module';
import { HttpClient } from '@angular/common/http';


describe('ProductEditComponent', () => {
  let component: ProductEditComponent;
  let fixture: ComponentFixture<ProductEditComponent>;
  let store: MockStore;
  let prodServiceSub$: ReplaySubject<Product>;
  let prodServiceSpy: jasmine.Spy;
  let routerSpy: jasmine.SpyObj<Router>;


  beforeEach(() => {

    prodServiceSub$ = new ReplaySubject<Product>();
    prodServiceSpy = jasmine.createSpyObj('ProdService', { getProductById: prodServiceSub$ });
    routerSpy = jasmine.createSpyObj('Router', ['navigate', 'navigateByUrl'], { url: 'mock/product/edit/new' });

    TestBed.configureTestingModule({
      imports: [ProductEditModule, RootStateModule],
      providers: [
        provideMockStore({
          initialState: {
            productEdit: { prodEditId: '', prodEditType: '', prodEditStage: 0 }
          }
        }),
        { provide: ProdService, useValue: prodServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: AuthService, useValue: {} },
        { provide: HttpClient, useValue: {} },
        { provide: BrandService, useValue: { get: () => EMPTY } },
        { provide: LineService, useValue: { get: () => EMPTY } },
        { provide: IngredientService, useValue: { get: () => EMPTY } },
        { provide: ComponentService, useValue: { get: () => EMPTY } },
        { provide: ActivatedRoute, useValue: { queryParams: EMPTY } },
      ]
    });

    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(ProductEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  describe('First stage', () => {

    it('should show form if prodEditStage is 1', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'MODIFY', prodEditStage: 1 });
      store.refreshState();
      prodServiceSub$.next(productStub);

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-first-stage-form')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-first-stage-view')).toBeFalsy();
        done();
      });

    });

    it('should show view if prodEditStage is NOT 1', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'MODIFY', prodEditStage: 2 });
      store.refreshState();
      prodServiceSub$.next(productStub);

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-first-stage-view')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-first-stage-form')).toBeFalsy();
        done();
      });

    });

  });

  describe('Second stage', () => {

    it('should show form if prodEditStage is 2', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'MODIFY', prodEditStage: 2 });
      store.refreshState();
      prodServiceSub$.next(productStub);

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-second-stage-form')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-second-stage-view')).toBeFalsy();
        done();
      });

    });

    it('should show view if prodEditStage is NOT 2', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'MODIFY', prodEditStage: 3 });
      store.refreshState();
      prodServiceSub$.next({ ...productStub, _stage: 2 });

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-second-stage-view')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-second-stage-form')).toBeFalsy();
        done();
      });

    });

    it('should show form if prodEditType is "NEW" and product _stage is 1', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'NEW', prodEditStage: 2 });
      store.refreshState();
      prodServiceSub$.next({ ...productStub, _stage: 1 });

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-second-stage-form')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-second-stage-view')).toBeFalsy();
        done();
      });

    });

    it('should show view if prodEditType is "NEW" and product _stage is greater than 1', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'NEW', prodEditStage: 3 });
      store.refreshState();
      prodServiceSub$.next({ ...productStub, _stage: 2 });

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-second-stage-view')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-second-stage-form')).toBeFalsy();
        done();
      });

    });

  });

  describe('Third stage', () => {

    it('should show form if prodEditStage is 3', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'MODIFY', prodEditStage: 3 });
      store.refreshState();
      prodServiceSub$.next(productStub);

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-third-stage-form')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-third-stage-view')).toBeFalsy();
        done();
      });

    });

    it('should show view if prodEditStage is NOT 3', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'MODIFY', prodEditStage: 2 });
      store.refreshState();
      prodServiceSub$.next({ ...productStub, _stage: 3 });

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-third-stage-view')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-third-stage-form')).toBeFalsy();
        done();
      });

    });

    it('should show form if prodEditType is "NEW" and product _stage === 2', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'NEW', prodEditStage: 3 });
      store.refreshState();
      prodServiceSub$.next({ ...productStub, _stage: 2 });

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-third-stage-form')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-third-stage-view')).toBeFalsy();
        done();
      });

    });

    it('should show view if prodEditType is "NEW" and product _stage is greater than 2', (done) => {
      store.overrideSelector(getProductEditState, { prodEditId: '0', prodEditType: 'NEW', prodEditStage: 4 });
      store.refreshState();
      prodServiceSub$.next({ ...productStub, _stage: 3 });

      component.vm$.subscribe(() => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-third-stage-view')).toBeTruthy();
        expect(fixture.nativeElement.querySelector('app-third-stage-form')).toBeFalsy();
        done();
      });

    });

  });

});


