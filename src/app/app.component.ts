import { Component, OnInit, TemplateRef, HostBinding, ViewChild, OnDestroy } from '@angular/core';
import { fromLeft, fromTop, triggerChildLeave } from './animations';
import { Router, NavigationEnd } from '@angular/router';
import { ModalService } from './service/modal.service';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';
import { ModalRef } from './share/modal/modal-ref';
import { Store } from '@ngrx/store';
import { isCompareEmpty, isCompareShown } from './ngrx/compare/compare.selectors';
import { hideComparePanel } from './ngrx/compare/compare.actions';
import { ReplaySubject } from 'rxjs';
import { getLoadingQueue } from './ngrx/main-spinner/main-spinner.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [triggerChildLeave, fromTop, fromLeft]
})

export class AppComponent implements OnInit, OnDestroy {

  @HostBinding('class') get glowyThemeName() { return this.glowyTheme ? 'glowy-theme' : 'gloomy-theme'; }

  @ViewChild('spinnerTpl') spinnerTpl!: TemplateRef<Element>;

  glowyTheme = true;
  showButton$ = this.appState.select(isCompareEmpty).pipe(map(isEmpty => !isEmpty));
  isCompareShown$ = this.appState.select(isCompareShown);

  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  private spinner: ModalRef | null = null;

  constructor(
    private appState: Store,
    private router: Router,
    private modalService: ModalService) { }

  ngOnInit(): void {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        window.scrollTo(0, 0);
        this.appState.dispatch(hideComparePanel());
      });

    this.appState.select(getLoadingQueue).pipe(
      takeUntil(this.destroy$),
      map((loadingNumber: number) => !!loadingNumber),
      distinctUntilChanged())
      .subscribe(val => this.toggleSpinner(val));

  }

  toggleSpinner(activate: boolean) {
    if (activate) {
      this.spinner = this.modalService.openSpinner({ content: this.spinnerTpl });
    }
    else if (this.spinner) {
      this.spinner.detach();
    }
  }

  listenToGlowyTheme() {
    this.glowyTheme = !this.glowyTheme;
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
