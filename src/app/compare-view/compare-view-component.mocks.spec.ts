import { initAnalysis } from '../model/analysis';
import { initFSDto } from '../model/fsdto';
import { Product } from '../model/product';
import { initSSDto } from '../model/ssdto';

export const mockProducts: Array<Product> = [
  {
    id: '1',
    fsDto: initFSDto(),
    ssDto: initSSDto(),
    tsDto: {
      ingQuotasList: [
        {
          ingredient: 'ing1_A',
          ingPerc: 80,
          compQuotasList: [
            {
              component: 'comp1_A_A',
              compPerc: 70,
            },
            {
              component: 'comp1_A_B',
              compPerc: undefined,
            }
          ],
        },
        {
          ingredient: 'ing1_B',
          ingPerc: undefined,
          compQuotasList: [
            {
              component: 'comp1_B_A',
              compPerc: 80,
            },
            {
              component: 'comp1_B_B',
              compPerc: undefined,
            },
            {
              component: 'comp1_B_C',
              compPerc: 30,
            }
          ],
        }
      ],
      analysis: initAnalysis(),
    },
    _stage: 3,
  },

  {
    id: '2',
    fsDto: initFSDto(),
    ssDto: initSSDto(),
    tsDto: {
      ingQuotasList: [
        {
          ingredient: 'ing2_A',
          ingPerc: 60,
          compQuotasList: [
            {
              component: 'comp2_A_A',
              compPerc: 80,
            }
          ],
        },
      ],
      analysis: initAnalysis(),
    },
    _stage: 3,
  },

  {
    id: '3',
    fsDto: initFSDto(),
    ssDto: initSSDto(),
    tsDto: {
      ingQuotasList: [
        {
          ingredient: 'ing3_A',
          ingPerc: 40,
          compQuotasList: [],
        },
        {
          ingredient: 'ing3_B',
          ingPerc: undefined,
          compQuotasList: [
            {
              component: 'comp3_B_A',
              compPerc: 40,
            },
            {
              component: 'comp3_B_B',
              compPerc: undefined,
            }
          ],
        },
        {
          ingredient: 'ing3_C',
          ingPerc: 20,
          compQuotasList: [
            {
              component: 'comp3_C_A',
              compPerc: 70,
            },
            {
              component: 'comp3_C_B',
              compPerc: undefined,
            }
          ],
        }
      ],
      analysis: initAnalysis(),
    },
    _stage: 3,
  }
];

export const expectedTransposedIngMatrix = [
  [
    {
      ingredient: 'ing1_A',
      ingPerc: 80,
      compQuotasList: [
        {
          component: 'comp1_A_A',
          compPerc: 70,
        },
        {
          component: 'comp1_A_B',
          compPerc: undefined,
        }
      ],
    },
    {
      ingredient: 'ing2_A',
      ingPerc: 60,
      compQuotasList: [
        {
          component: 'comp2_A_A',
          compPerc: 80,
        }
      ],
    },
    {
      ingredient: 'ing3_A',
      ingPerc: 40,
      compQuotasList: [],
    }
  ],

  [
    {
      ingredient: 'ing1_B',
      ingPerc: undefined,
      compQuotasList: [
        {
          component: 'comp1_B_A',
          compPerc: 80,
        },
        {
          component: 'comp1_B_B',
          compPerc: undefined,
        },
        {
          component: 'comp1_B_C',
          compPerc: 30,
        }
      ],
    },
    undefined,
    {
      ingredient: 'ing3_B',
      ingPerc: undefined,
      compQuotasList: [
        {
          component: 'comp3_B_A',
          compPerc: 40,
        },
        {
          component: 'comp3_B_B',
          compPerc: undefined,
        }
      ],
    },
  ],

  [
    undefined,
    undefined,
    {
      ingredient: 'ing3_C',
      ingPerc: 20,
      compQuotasList: [
        {
          component: 'comp3_C_A',
          compPerc: 70,
        },
        {
          component: 'comp3_C_B',
          compPerc: undefined,
        }
      ],
    }
  ]
];

