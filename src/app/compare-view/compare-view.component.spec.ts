import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { getComparedProducts, isCompareEmpty } from '../ngrx/compare/compare.selectors';
import { expectedTransposedIngMatrix, mockProducts } from './compare-view-component.mocks.spec';
import { CompareViewComponent } from './compare-view.component';
import { CompareViewModule } from './compare-view.module';

describe('CompareViewComponent', () => {
  let component: CompareViewComponent;
  let fixture: ComponentFixture<CompareViewComponent>;
  let store: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ CompareViewModule ],
      providers: [ provideMockStore({ initialState: { compare: { products: [] } } }) ]
    });

    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(CompareViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should return transposed products ingredient matrix', (done) => {
    store.overrideSelector(getComparedProducts, mockProducts);
    store.overrideSelector(isCompareEmpty, false);

    store.refreshState();
    const expectedTransp = expectedTransposedIngMatrix;

    component.vm$.subscribe(value => {
      expect(value).toEqual(
        {
          products: mockProducts,
          transposedIngMatrix: expectedTransp,
          compareEmpty: false
        }
      );
      done();
    });
  });

});
