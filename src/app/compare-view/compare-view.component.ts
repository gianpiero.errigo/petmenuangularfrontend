import { Component } from '@angular/core';
import { Product } from '../model/product';
import { faTimesCircle } from '@fortawesome/free-regular-svg-icons';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';
import { fromRight } from '../animations';
import { Store } from '@ngrx/store';
import { getComparedProducts, isCompareEmpty } from '../ngrx/compare/compare.selectors';
import { map } from 'rxjs/operators';
import { clearCompare, rmFromCompare } from '../ngrx/compare/compare.actions';
import { combineLatest } from 'rxjs';
import { Analysis } from '../model/analysis';
import { IngredientQuota } from '../model/ingredient-quota';

@Component({
  selector: 'app-compare-view',
  templateUrl: './compare-view.component.html',
  styleUrls: ['./compare-view.component.scss'],
  animations: [fromRight]
})

export class CompareViewComponent {

  vm$ = combineLatest([
    this.appState.select(getComparedProducts).pipe(
      map(products => ({products, transposedIngMatrix: this.getTransposedIngMatrix(products)}))),
    this.appState.select(isCompareEmpty)
  ]).pipe(map(([{products, transposedIngMatrix}, compareEmpty]) => ({products, transposedIngMatrix, compareEmpty})));

  imgsDir = environment.pmImgs;
  iconClose = faTimesCircle;
  iconClear = faTrashAlt;

  faClose = faTimesCircle;

  _elements: Array<[keyof Analysis, string]> = [
    ['rawProtein', $localize`:compare@@protein:Protein`],
    ['rawFibre', $localize`:compare@@fibre:Fibre`],
    ['rawOilFat', $localize`:compare@@oilfat:Oil and Fat`],
    ['rawAshes', $localize`:compare@@ashes:Ashes`],
    ['calcium', $localize`:compare@@calcium:Calcium`],
    ['sodium', $localize`:compare@@sodium:Sodium`],
    ['phosphorus', $localize`:compare@@phosphorus:Phosphorus`]
  ];

  iconGoto = faAngleDoubleRight;

  constructor(public appState: Store) { }

  public getTransposedIngMatrix(products: Array<Product>): Array<Array<IngredientQuota|undefined>> {
    if (products.length > 0) {
      const ingMatrix = products.map((prod: Product) =>  (prod.tsDto) ? prod.tsDto.ingQuotasList : []);
      return Array.from({ length: Math.max(...ingMatrix.map(row => row.length)) }, (_, i) => ingMatrix.map(row => row[i]));
    } else {
      return [];
    }
  }

  public clearCompareList() {
    return this.appState.dispatch(clearCompare());
  }

  public rmProduct(product: Product) {
    return this.appState.dispatch(rmFromCompare({product}));
  }

}
