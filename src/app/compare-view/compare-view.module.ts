import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CompareViewComponent } from './compare-view.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TouchTooltipModule } from '../share/modal/touch-tooltip/touch-tooltip.module';


@NgModule({
  declarations: [
    CompareViewComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    TouchTooltipModule
  ],
  exports: [
    CompareViewComponent,
  ]
})
export class CompareViewModule { }
