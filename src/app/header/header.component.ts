import { Component, Output, EventEmitter, Input } from '@angular/core';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faAdjust, faHome, faReply } from '@fortawesome/free-solid-svg-icons';
import { fromTop, dropDown } from '../animations';
import { Router } from '@angular/router';
import { LocationStrategy } from '@angular/common';
import { NavigationService } from '../service/navigation.service';
import { AuthService } from '@auth0/auth0-angular';
import { filter, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { AuthManagementService } from '../service/auth-management.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [fromTop, dropDown]
})
export class HeaderComponent {

  @Output() tellGlowyTheme: EventEmitter<void> = new EventEmitter<void>();
  @Input() glowyTheme = true;

  user$ = this.auth.idTokenClaims$.pipe(
    filter(claims => !!claims),
    map(claims =>
      ({
        safeSub: claims?.['https://petmenu.eu/URISafeSub'],
        petmenuUsername: claims?.['https://petmenu.eu/petmenu_username']
      })
    )
  );

  iconContrast = faAdjust;
  iconUser = faUser;
  iconHome = faHome;
  iconBack = faReply;

  newName: FormControl = new FormControl('');

  constructor(public auth: AuthService,
    private authManagement: AuthManagementService,
    private router: Router,
    protected locationStrategy: LocationStrategy,
    public navService: NavigationService) {

  }

  logIn() {
    //Hack to allow correct back button redirect on login prompt https://issues.redhat.com/browse/KEYCLOAK-15463
    //    window.history.pushState(window.history.state, document.title, window.location.href);
    /*************** */
    this.auth.loginWithRedirect({ appState: { target: this.router.routerState.snapshot.url } });
  }

  logOut() {
    this.auth.logout({ returnTo: `${location.origin}${this.locationStrategy.prepareExternalUrl('')}` });
  }

  switchTheme() {
    this.tellGlowyTheme.emit();
  }

  changeName() {
    this.authManagement.changeName(this.newName.value).subscribe();
  }
}
