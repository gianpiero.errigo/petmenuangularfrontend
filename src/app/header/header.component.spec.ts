//import { Directive } from '@angular/core';
//import { ComponentFixture, TestBed } from '@angular/core/testing';
//import { By } from '@angular/platform-browser';
//import { RouterTestingModule } from '@angular/router/testing';
//import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
//
//import { NavigationService } from '../service/navigation.service';
//
//import { HeaderComponent } from './header.component';
//import { HeaderModule } from './header.module';
//
//describe('HeaderComponent', () => {
//  let component: HeaderComponent;
//  let fixture: ComponentFixture<HeaderComponent>;
//  let authSpy: jasmine.Spy;
//
//	const authMock = {
//		getKeycloakInstance: () => ({
//			authenticated: null,
//			loadUserProfile() {return null}
//		})
//	};
//
//	@Directive({ selector: '[appDropdown]' })
//  class AppDropdownStubDirective {}
//
//  beforeEach(() => {
//		TestBed.configureTestingModule({
//			declarations: [ AppDropdownStubDirective ],
//      providers: [
//									{provide: KeycloakService, useValue: authMock},
//      						{provide: NavigationService, useValue: {}},
//      					],
//      imports: [HeaderModule, RouterTestingModule, KeycloakAngularModule]
//    });
//
//    fixture = TestBed.createComponent(HeaderComponent);
//    component = fixture.componentInstance;
//    fixture.detectChanges();
//    const authService = TestBed.inject(KeycloakService);
//		authSpy = spyOn(authService, 'getKeycloakInstance');
//	});
//
//  it('should apply DropDown and light class to icon after login', (done) => {
//		const buttons = fixture.debugElement.queryAllNodes(By.directive(AppDropdownStubDirective));
//		authSpy.and.returnValue({authenticated: true, loadUserProfile() { return Promise.resolve('Admin') }});
//		component.ngOnInit();
//		fixture.detectChanges();
//		authSpy.calls.mostRecent().returnValue.loadUserProfile().then(() => {
//			fixture.detectChanges();
//			expect(fixture.debugElement.nativeElement.querySelector('fa-icon')).toHaveClass('light-svg');
//			expect(buttons.length).toBe(1);
//			done();
//		});
//  });
//
////  it('should apply DropDown and dark class to icon if not logged in', (done) => {
////		const icon = fixture.debugElement.nativeElement.querySelector('fa-icon');
////		const buttons = fixture.debugElement.queryAllNodes(By.directive(AppDropdownStubDirective));
////		authSpy.and.returnValue({authenticated: false, loadUserProfile() { return Promise.reject() }});
////		component.ngOnInit();
////		authSpy.calls.mostRecent().returnValue.loadUserProfile().then(() => {
////			fixture.detectChanges();
////			expect(fixture.debugElement.nativeElement.querySelector('fa-icon')).toHaveClass('dark-svg');
////			expect(buttons.length).toBe(1);
////			done();
////		});
////  });
//
//});
