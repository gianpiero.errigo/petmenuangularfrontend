import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HasRoleModule } from '../share/directives/has-role.module';
import { DropdownModule } from '../share/modal/dropdown/dropdown.module';
import { HeaderComponent } from './header.component';


@NgModule({
  declarations: [
    HeaderComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    DropdownModule,
    HasRoleModule
  ],
  exports: [
    HeaderComponent,
  ]
})
export class HeaderModule { }
