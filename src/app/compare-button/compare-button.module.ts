import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompareButtonComponent } from './compare-button.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveComponentModule } from '@ngrx/component';


@NgModule({
  declarations: [
    CompareButtonComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveComponentModule,
  ],
  exports: [
    CompareButtonComponent,
  ]
})
export class CompareButtonModule { }
