import { Component } from '@angular/core';
import { faExchangeAlt } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { compButton, fromRight, wobbleVertRight } from '../animations';
import { toggleComparePanel } from '../ngrx/compare/compare.actions';
import { getCompareLength } from '../ngrx/compare/compare.selectors';
import { CompareProductsService } from '../service/compare-products.service';

@Component({
  selector: 'app-compare-button',
  templateUrl: './compare-button.component.html',
  styleUrls: ['./compare-button.component.scss'],
  animations: [compButton, fromRight, wobbleVertRight]
})
export class CompareButtonComponent {

  compareLength$ = this.appState.select(getCompareLength);
  iconOpenClose = faExchangeAlt;

  constructor(private appState: Store,
		public compareService: CompareProductsService) {}

  toggleComparePanel() {
    this.appState.dispatch(toggleComparePanel());
  }

}
