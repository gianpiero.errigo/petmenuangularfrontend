import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { of, Subject } from 'rxjs';
import { CompareProductsService } from '../service/compare-products.service';
import { CompareButtonComponent } from './compare-button.component';
import { CompareButtonModule } from './compare-button.module';

describe('CompareButtonComponent', () => {
  let component: CompareButtonComponent;
  let fixture: ComponentFixture<CompareButtonComponent>;
  const SELECTION_LENGTH = 4;

  const storeMock = {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    select: (_: string) => of(SELECTION_LENGTH)
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CompareButtonModule],
      providers: [
        { provide: Store, useValue: storeMock },
        { provide: CompareProductsService, useValue: { wobbleButton$: new Subject<boolean>() } }
      ]
    });

    fixture = TestBed.createComponent(CompareButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display as many lighten-up rectangles as selected products', () => {
    fixture.detectChanges();
    const rects = fixture.debugElement.queryAll(By.css('.pet-rectangle'));
    expect(rects.length).toBe(4);
    expect(rects.filter((debEl: DebugElement) => !!debEl.classes['pet-animate-heartbeat-on']).length).toBe(SELECTION_LENGTH);
  });
});
