import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'authCallback', component: AuthCallbackComponent, pathMatch: 'full' },
  
  { 
    path: 'home',
    component: HomeComponent,
    children: [
      
      { 
        path: 'search', 
        loadChildren: () => import(`./home/search-main-form/search-main-form.module`).then(m => m.SearchMainFormModule),
      },
      
      { 
        path: 'addnew', 
        loadChildren: () => import(`./home/add-new/add-new.module`).then(m => m.AddNewModule),
      },
      
      { 
        path: 'incompletes',
        loadChildren: () => import(`./home/incompletes/incompletes.module`).then(m => m.IncompletesModule),
      }
    
    ]
  },

  { path: 'product', loadChildren: () => import(`./product/product.module`).then(m => m.ProductModule) },

  {	path: 'user', loadChildren: () => import(`./user/user.module`).then(m => m.UserModule) },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
