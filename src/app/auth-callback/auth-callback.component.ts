import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { addLoading } from '../ngrx/main-spinner/main-spinner.actions';

@Component({
  selector: 'app-auth-callback',
  template: '',
  styleUrls: ['./auth-callback.component.scss']
})
export class AuthCallbackComponent {

  constructor(private appState: Store) {
    this.appState.dispatch(addLoading());
  }


}
