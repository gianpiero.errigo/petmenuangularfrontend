import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchMainFormComponent } from '../search-main-form/search-main-form.component';

import { HomeComponent } from './home.component';
import { HomeModule } from './home.module';

@Component({
  selector: 'app-search-main-form',
  template: `<div>Mock</div>`
})
class SearchMainFormMockComponent { }

describe('HomeComponent', () => {

  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let buttons: DebugElement[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SearchMainFormMockComponent],
      imports: [HomeModule, RouterTestingModule],
    }).overrideComponent(HomeComponent, {
      set: {
        providers: [{ provide: SearchMainFormComponent, useClass: SearchMainFormMockComponent }]
      }
    });

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    buttons = fixture.debugElement.queryAll(By.css('button'));
    //		factoryRes = TestBed.inject(ComponentFactoryResolver);
    //
    //		factorySpy = spyOn(factoryRes, 'resolveComponentFactory').and.callThrough();

  });

  //  it('should show/hide "search" component on button click', fakeAsync(() => {
  //    const button = buttons.find(b => b.nativeElement.innerHTML === 'Search');
  //    button.triggerEventHandler('click', null);
  //    tick();
  //    expect(factorySpy).toHaveBeenCalledOnceWith('search');
  //		fixture.whenRenderingDone().then(() => {
  //    expect(fixture.debugElement.query(By.css('app-search-main-form'))).toBeTruthy();
  //		});
  //  }));

  it('should show/hide "new" template on button click', fakeAsync(() => {
    const button = buttons.find(b => b.nativeElement.innerText === 'New');
    button?.triggerEventHandler('click', null);
    tick();
    fixture.whenStable().then(() => {
      const tplHeader: HTMLElement = fixture.debugElement.query(By.css('h2')).nativeElement;
      expect(tplHeader.innerText).toContain('Aggiungi un nuovo prodotto');
    });
  }));

  it('should show/hide "help us" template on button click', fakeAsync(() => {
    const button = buttons.find(b => b.nativeElement.innerText === 'Help us');
    button?.triggerEventHandler('click', null);
    tick();
    fixture.whenStable().then(() => {
      const tplHeader: HTMLElement = fixture.debugElement.query(By.css('h2')).nativeElement;
      expect(tplHeader.innerText).toContain('Completa i prodotti parziali');
    });
  }));
});
