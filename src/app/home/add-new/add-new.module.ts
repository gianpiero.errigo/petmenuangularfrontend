import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { AddNewComponent } from "./add-new.component";

const routes: Routes = [
  { path: '', component: AddNewComponent }
];

@NgModule({
  declarations: [
    AddNewComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    AddNewComponent,
  ]
})
export class AddNewModule { }