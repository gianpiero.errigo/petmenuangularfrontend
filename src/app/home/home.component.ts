import { Component } from '@angular/core';
import { homeAnimationSequence, fromLeft, fromRight } from '../animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [homeAnimationSequence, fromLeft, fromRight]
})
export class HomeComponent {

  animState = '';
  
}

