import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SearchMainFormComponent } from './search-main-form.component';

const routes: Routes = [
  { path: '', component: SearchMainFormComponent }
];

@NgModule({
  declarations: [
    SearchMainFormComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    SearchMainFormComponent,
  ]
})
export class SearchMainFormModule { }
