import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { merge, Observable, ReplaySubject } from 'rxjs';
import { filter, distinctUntilChanged, takeUntil, switchMap, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LineService } from '../../service/line.service';
import {
  prodTypes, prodPackages, locProdTypes, locProdPackages,
  isProdType, isProdPackage
} from 'src/app/model/enums';
import { BrandService } from 'src/app/service/brand.service';
import { fromBottom } from 'src/app/animations';
import { ViewChild } from '@angular/core';
import { ModalService } from '../../service/modal.service';
import { Top } from '../../model/modal-positions';
import { emptySearchMsg } from 'src/assets/msgs/tooltip-msgs';

@Component({
  selector: 'app-search-main-form',
  templateUrl: './search-main-form.component.html',
  animations: [fromBottom]
})
export class SearchMainFormComponent implements OnInit, OnDestroy {

  @ViewChild('searchBtn') private searchBtn!: ElementRef;

  prodTypes = [...prodTypes];
  locProdTypes = { ...locProdTypes };
  prodPackages = [...prodPackages];
  locProdPackages = { ...locProdPackages };

  searchMainForm: FormGroup;
  weightRange: FormControl = new FormControl(0);

  #lines: string[] = [];
  set lines(newLines: string[]) {
    this.#lines = newLines;
    if (newLines.length === 0) {
      this.searchMainForm.get('line')?.disable();
    } else {
      this.searchMainForm.get('line')?.enable();
    }
  }

  get lines() { return this.#lines; }

  weightRanges = [
    { name: $localize`:opt@@anyWeightRanges:--Any--`, min: 0, max: 0 },
    { name: '0gr-150gr', min: 0, max: 150 },
    { name: '150gr-500gr', min: 150, max: 500 },
    { name: '500gr-1Kg', min: 500, max: 1000 },
    { name: '1Kg-3Kg', min: 1000, max: 3000 },
    { name: '3Kg+', min: 3000, max: 0 }
  ];

  brands$: Observable<string[]> = new Observable();

  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(private router: Router,
    private brandService: BrandService,
    private lineService: LineService,
    private formBuilder: FormBuilder,
    private modalService: ModalService) {
    this.searchMainForm = this.formBuilder.group({
      brand: [''],
      line: [{ value: '', disabled: true }],
      ing: [''],
      comp: [''],
      prodType: [''],
      prodPackage: [''],
      minWeight: [0],
      maxWeight: [0]
    });
  }

  ngOnInit(): void {

    this.brands$ = this.brandService.get();

    this.searchMainForm.get('brand')?.valueChanges.pipe(
      filter(_brand => _brand !== undefined),
      takeUntil(this.destroy$),
      distinctUntilChanged(),
      switchMap((_brand: string) => this.lineService.getFromBrand(_brand)))
      .subscribe((data: string[]) => this.lines = data);

    this.weightRange.valueChanges.pipe(
      takeUntil(this.destroy$),
      map((index: number) => this.weightRanges[index]))
      .subscribe(({ min, max }) => {
        this.searchMainForm.get('minWeight')?.setValue(min, { emitEvent: false });
        this.searchMainForm.get('maxWeight')?.setValue(max, { emitEvent: false });
      });

    merge([
      this.searchMainForm.get('minWeight')?.valueChanges,
      this.searchMainForm.get('maxWeight')?.valueChanges
    ]).pipe(
      //TODO: waiting for typed forms to reset maxweigth
      //      tap(([min, max]: [number, number]) => min > max ? this.searchMainForm.get('maxWeight')?.reset()),
      takeUntil(this.destroy$))
      .subscribe(() => this.weightRange.setValue(0, { emitEvent: false }));
  }

  onSubmit() {
    //TODO: to be refactored after upgrade to typed forms

    //		const sanitizedParams = this.sanitizeSubmit();
    if (Object.keys(this.searchMainForm.value).length !== 0) { //sanitizedParams).length !== 0) {
      this.router.navigate(['product/search'], { queryParams: { ...this.searchMainForm.value } });//{ ...sanitizedParams }});
    }
    else {
      this.modalService.openTooltip({
        origin: this.searchBtn,
        positions: [Top],
        offsetY: -20,
        content: emptySearchMsg.header,
      });
    }
  }

  //TODO: to be refactored after upgrade to typed forms
  //	sanitizeSubmit(): Required<SearchParams> {
  //
  //		const reduced = Object.entries<string>(this.searchMainForm.value)
  //			.reduce<Required<SearchParams>>((obj, [key, value]) => {
  //          if(key === 'prodType' && isProdType(value)) {
  //            obj.prodType = value;
  //          }
  //          else if(key === 'prodPackage' && isProdPackage(value)) {
  //            obj.prodPackage = value;
  //          }
  //  				else {
  //  					value.trim();
  //            const k: keyof SearchParams = key.valueOf as keyof SearchParams;
  //            obj[k] = value;
  //          }
  //  			return obj;
  //  		}, {} as Required<SearchParams>);
  //		return reduced as Required<SearchParams>;
  //
  //	}

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
