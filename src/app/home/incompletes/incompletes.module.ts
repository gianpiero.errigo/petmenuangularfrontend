import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { IncompletesComponent } from "./incompletes.component";

const routes: Routes = [
  { path: '', component: IncompletesComponent }
];

@NgModule({
  declarations: [
    IncompletesComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
  ],
  exports: [
    IncompletesComponent,
  ]
})
export class IncompletesModule { }