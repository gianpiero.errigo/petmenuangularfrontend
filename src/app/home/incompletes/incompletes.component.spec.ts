import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncompletesComponent } from './incompletes.component';

describe('IncompletesComponent', () => {
  let component: IncompletesComponent;
  let fixture: ComponentFixture<IncompletesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncompletesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncompletesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
