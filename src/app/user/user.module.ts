import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ListUserCommitsModule } from './list-user-commits/list-user-commits.module';
import { ListUsersModule } from './list-users/list-users.module';


@NgModule({
  declarations: [],
  imports: [
    ListUsersModule,
    ListUserCommitsModule,
    CommonModule,
    FontAwesomeModule,
    UserRoutingModule,
    NgbPaginationModule
  ],
})
export class UserModule { }
