import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserCommit } from 'src/app/model/user-commit';
import { ListUserCommitsService } from 'src/app/service/list-user-commits.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { faSortAmountDown, faSortAmountDownAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-list-user-commits',
  templateUrl: './list-user-commits.component.html',
  styleUrls: ['./list-user-commits.component.css']
})

export class ListUserCommitsComponent implements OnInit, OnDestroy {

  commitsList: UserCommit[] = [];
  sortedBy = '';
  ascendingOrder = false;

  page = 1;
  pageSize = 20;

  iconSortDesc = faSortAmountDown;
  iconSortAsc = faSortAmountDownAlt;

  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);


  constructor(private lucService: ListUserCommitsService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.params.pipe(
      takeUntil(this.destroy$),
      switchMap(({ userName }) => this.lucService.get(userName)))
      .subscribe((ucList: Array<UserCommit>) => this.commitsList = ucList);
  }

  onSortBy(criteria: keyof UserCommit) {
    if (this.sortedBy !== criteria) {
      if (criteria === 'time') {
        this.commitsList.sort((a, b) => b[criteria].getTime() - a[criteria].getTime());
      }
      else if (criteria === 'ucType') {
        const ucNewList = this.commitsList.filter(uc => uc.ucType === 'NEW');
        const ucEditList = this.commitsList.filter(uc => !ucNewList.includes(uc));

        this.commitsList = ucNewList.concat(ucEditList);
      }
      else {
        this.commitsList.sort((a, b) => b[criteria] - a[criteria]);
      }
      this.sortedBy = criteria;
    }
    else {
      this.commitsList.reverse();
      this.ascendingOrder = !this.ascendingOrder;
    }
  }

  onPageChange() {
    window.scrollTo(0, 0);
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
