import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ListUserCommitsComponent } from './list-user-commits.component';


@NgModule({
  declarations: [
    ListUserCommitsComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    NgbPaginationModule
  ],
  exports: [
    ListUserCommitsComponent,
  ]
})
export class ListUserCommitsModule { }
