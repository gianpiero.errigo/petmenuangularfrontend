import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUsersComponent } from './list-users/list-users.component';
import { ListUserCommitsComponent } from './list-user-commits/list-user-commits.component';
import { AuthGuard } from '@auth0/auth0-angular';


const routes: Routes = [

  {
    path: 'list',
    component: ListUsersComponent,
    canActivate: [AuthGuard]
  },

  {
    path: ':userName/commitslist',
    component: ListUserCommitsComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule { }
