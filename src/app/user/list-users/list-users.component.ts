import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { ReplaySubject } from 'rxjs';
import { ListUsersService } from 'src/app/service/list-users.service';
import { faSortAmountDownAlt, faSortAmountDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit, OnDestroy {

  usersList: User[] = [];
  destroy$: ReplaySubject<boolean> = new ReplaySubject(1);

  sortedBy = '';
  ascendingOrder = false;
  page = 1;
  pageSize = 3;

  iconSortDesc = faSortAmountDown;
  iconSortAsc = faSortAmountDownAlt;

  constructor(private luService: ListUsersService) { }

  ngOnInit(): void {

    this.luService.get()
      .subscribe((userList: Array<User>) => this.usersList = userList);

  }

  onSortBy(criteria: keyof User) {
    if (this.sortedBy !== criteria) {
      if (criteria === 'lastCommit') {
        this.usersList.sort((a, b) => b[criteria].getTime() - a[criteria].getTime());
      }
      else {
        this.usersList.sort((a, b) => Number(b[criteria]) - Number(a[criteria]));
      }
      this.sortedBy = criteria;
    }
    else {
      this.usersList.reverse();
      this.ascendingOrder = !this.ascendingOrder;
    }
  }

  onPageChange() {
    window.scrollTo(0, 0);
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
