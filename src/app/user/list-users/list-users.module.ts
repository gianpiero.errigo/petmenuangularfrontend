import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ListUsersComponent } from './list-users.component';


@NgModule({
  declarations: [
    ListUsersComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    NgbPaginationModule
  ],
  exports: [
    ListUsersComponent,
  ]
})
export class ListUsersModule { }
