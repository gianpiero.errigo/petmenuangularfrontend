import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { authConfig } from 'src/environments/environment';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RootStateModule } from './ngrx/root-state-module';
import { CompareButtonModule } from './compare-button/compare-button.module';
import { CompareViewModule } from './compare-view/compare-view.module';
import { FooterModule } from './footer/footer.module';
import { HeaderModule } from './header/header.module';
import { HomeModule } from './home/home.module';
import { AuthHttpInterceptor, AuthModule } from '@auth0/auth0-angular';
import { HasRoleModule } from './share/directives/has-role.module';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { StripSearchParamsInterceptor } from './interceptor/strip-search-params-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    AuthCallbackComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RootStateModule,
    HttpClientModule,
    FontAwesomeModule,
    CompareButtonModule,
    CompareViewModule,
    HeaderModule,
    FooterModule,
    HomeModule,
    HasRoleModule,
    AuthModule.forRoot(authConfig),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: StripSearchParamsInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]

})
export class AppModule {}
