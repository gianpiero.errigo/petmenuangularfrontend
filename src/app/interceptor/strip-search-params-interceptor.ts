import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class StripSearchParamsInterceptor implements HttpInterceptor {

  /**
   * Iterate through query parameters and remove all those that are `undefined`.
   *
   * @param request The incoming request.
   * @param next The next handler.
   * @returns The handled request.
   */
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(request.url.endsWith(`${environment.pmBackUrl}/product/search/`)) {
      let params = request.params;
      for (const key of request.params.keys()) {
        if (!params.get(key) || params.get(key) === '0') {
          params = params.delete(key);
        }
      }
      request = request.clone({ params });
    }
    return next.handle(request);
  }
}
