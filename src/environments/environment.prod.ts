import { AuthConfig } from '@auth0/auth0-angular';

const API_SERVER = 'https://79.9.147.80:8443';

export const environment = {
  production: true,
  pmBackUrl: API_SERVER,
  pmImgs: 'http://79.9.147.80:9090/imgs'
};

export const authConfig: AuthConfig = {
  domain: 'petmenu-dev.eu.auth0.com',
  clientId: '0acSxbyGpAv6wfOWZEHx45fzfU8CmiHR',
  redirectUri: `${window.location.origin}/dev/authCallback`,
  audience: 'http://79.9.147.80',
  scope: 'input:product delete:product read:users read:commitList',
  httpInterceptor: {
    allowedList: [
      {
        httpMethod: 'PATCH',
        uri: `${API_SERVER}/auth/*`
      },
      {
        httpMethod: 'POST',
        uri: `${API_SERVER}/product/*`
      },
      {
        httpMethod: 'PUT',
        uri: `${API_SERVER}/product/*`
      },
      {
        httpMethod: 'GET',
        uri: `${API_SERVER}/user/*`
      },
    ]
  }
};
