import { ModalMsg } from 'src/app/model/modal-params';

export const prodDeleteSuccessInfoMsg: ModalMsg = {
  header: $localize`:info@@deleteSuccessHeader:Product deleted`,
  body: $localize`:info@@deleteSuccessBody:Product has been correctly deleted from database`
};

export const prodDeleteFailInfoMsg: ModalMsg = {
  header: $localize`:info@@deleteFailHeader:Product deletion failed`,
  body: $localize`:info@@deleteFailBody:Product has NOT been deleted from database`
};
