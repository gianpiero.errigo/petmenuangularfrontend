import { ModalMsg } from 'src/app/model/modal-params';

export const prodDeleteConfirmMsg: ModalMsg = {
  header: $localize`:confirm@@productDeletionHeader:Cancellazione prodotto`,
  body: $localize`:confirm@@productDeletionBody:Confermi di voler eliminare definitivamente il prodotto selezionato?`
};

export const prodConflictMsg: ModalMsg = {
  header: $localize`:confirm@@alreadyExistHeader:Il prodotto è già presente nel Database`,
  body: $localize`:confirm@@alreadyExistBody:In base ai dati inseriti il prodotto risulta già presente nel database di PetMenu\n 
								È possibile confermare i dati e visualizzare il prodotto già esistente 
								oppure annullare l'operazione per revisionarli`
};
