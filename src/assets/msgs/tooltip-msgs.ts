import { ModalMsg } from 'src/app/model/modal-params';

export const compareListFullMsg: ModalMsg = {
  header: $localize`:info@@compareListFullHeader:Cannot select more than 4 elements to compare`,
};

export const emptySearchMsg: ModalMsg = {
  header: $localize`:info@@emptySearchMsgHeader:Insert at least one search criteria`,
};

